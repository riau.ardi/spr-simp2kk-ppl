<?php
include('admin-side/config.php');

$res_senin  = mysqli_query($koneksi, "SELECT nama_petugas, bidang_petugas, jam_kerja FROM petugas WHERE jadwal_petugas LIKE '%Senin%';") or die(mysqli_error($koneksi));
$res_selasa = mysqli_query($koneksi, "SELECT nama_petugas, bidang_petugas, jam_kerja FROM petugas WHERE jadwal_petugas LIKE '%Selasa%';") or die(mysqli_error($koneksi));
$res_rabu   = mysqli_query($koneksi, "SELECT nama_petugas, bidang_petugas, jam_kerja FROM petugas WHERE jadwal_petugas LIKE '%Rabu%';") or die(mysqli_error($koneksi));
$res_kamis  = mysqli_query($koneksi, "SELECT nama_petugas, bidang_petugas, jam_kerja FROM petugas WHERE jadwal_petugas LIKE '%Kamis%';") or die(mysqli_error($koneksi));
$res_jumat  = mysqli_query($koneksi, "SELECT nama_petugas, bidang_petugas, jam_kerja FROM petugas WHERE jadwal_petugas LIKE '%Jumat%';") or die(mysqli_error($koneksi));
$res_sabtu  = mysqli_query($koneksi, "SELECT nama_petugas, bidang_petugas, jam_kerja FROM petugas WHERE jadwal_petugas LIKE '%Sabtu%';") or die(mysqli_error($koneksi));


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Jadwal Praktik</title>
    <link rel="icon" type="image/x-icon" href="admin-side/assets/images/logo-small.png" />
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="css/styles.css" rel="stylesheet" />
</head>

<body>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
        <div class="container px-4 px-lg-5">
            <a class="navbar-brand" href="#"> </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto py-4 py-lg-0">
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="index.php">Home</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="daftar.php">Daftar Antrian</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="jadwal.php">Jadwal Praktik</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="about.php">Tentang Puskesmas</a></li>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('admin-side/assets/images/kalender.jfif')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <p class="subheading">Puskesmas II Karang Klesem</p>
                        <h2>Jadwal Praktek Dokter</h2>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Post Content-->
    <article class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-10">
                    <div id="accordion">
                        <!-- button senin -->
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <a class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#senin" aria-expanded="false" aria-controls="senin">
                                        Senin
                                    </a>
                                </h5>
                            </div>
                            <!-- content senin -->
                            <div id="senin" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <div class='table-responsive-sm'>
                                        <table class='table table-sm' style="width: 100%;">
                                            <tbody>
                                                <?php
                                                if (mysqli_num_rows($res_senin) > 0) {
                                                    while ($data = mysqli_fetch_assoc($res_senin)) {
                                                        echo "                                    
                                                        <tr>
                                                            <td style='width: 70%;'>
                                                                <sub>" . $data['bidang_petugas'] . "</sub> <br>
                                                                " . $data['nama_petugas'] . "
                                                            </td>
                                                            <td>
                                                                <sub> </sub> <br>
                                                                " . $data['jam_kerja'] . "
                                                            </td>
                                                        </tr>";
                                                    }
                                                } else {
                                                    echo "0 results";
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- button selasa -->
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <a class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#selasa" aria-expanded="false" aria-controls="selasa">
                                        Selasa
                                    </a>
                                </h5>
                            </div>
                            <!-- content selasa -->
                            <div id="selasa" class="collapse show" aria-labelledby="headingSelasa" data-parent="#accordion">
                                <div class="card-body">
                                    <div class='table-responsive-sm'>
                                        <table class='table table-sm' style="width: 100%;">
                                            <tbody>
                                                <?php
                                                if (mysqli_num_rows($res_selasa) > 0) {
                                                    while ($data = mysqli_fetch_assoc($res_selasa)) {
                                                        echo "
                                            
                                                        <tr>
                                                            <td style='width: 70%;'>
                                                                <sub>" . $data['bidang_petugas'] . "</sub> <br>
                                                                " . $data['nama_petugas'] . "
                                                            </td>
                                                            <td>
                                                                <sub> </sub> <br>
                                                                " . $data['jam_kerja'] . "
                                                            </td>
                                                        </tr>";
                                                    }
                                                } else {
                                                    echo "0 results";
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- button rabu -->
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <a class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#rabu" aria-expanded="false" aria-controls="rabu">
                                        Rabu
                                    </a>
                                </h5>
                            </div>
                            <!-- content rabu -->
                            <div id="rabu" class="collapse show" aria-labelledby="headingRabu" data-parent="#accordion">
                                <div class="card-body">
                                    <div class='table-responsive-sm'>
                                        <table class='table table-sm' style="width: 100%;">
                                            <tbody>
                                                <?php
                                                if (mysqli_num_rows($res_rabu) > 0) {
                                                    while ($data = mysqli_fetch_assoc($res_rabu)) {
                                                        echo "
                                            
                                                        <tr>
                                                            <td style='width: 70%;'>
                                                                <sub>" . $data['bidang_petugas'] . "</sub> <br>
                                                                " . $data['nama_petugas'] . "
                                                            </td>
                                                            <td>
                                                                <sub> </sub> <br>
                                                                " . $data['jam_kerja'] . "
                                                            </td>
                                                        </tr>";
                                                    }
                                                } else {
                                                    echo "0 results";
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- button kamis -->
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <a class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#kamis" aria-expanded="false" aria-controls="kamis">
                                        Kamis
                                    </a>
                                </h5>
                            </div>
                            <!-- content kamis -->
                            <div id="kamis" class="collapse show" aria-labelledby="headingKamis" data-parent="#accordion">
                                <div class="card-body">
                                    <div class='table-responsive-sm'>
                                        <table class='table table-sm' style="width: 100%;">
                                            <tbody>
                                                <?php
                                                if (mysqli_num_rows($res_kamis) > 0) {
                                                    while ($data = mysqli_fetch_assoc($res_kamis)) {
                                                        echo "                                            
                                                        <tr>
                                                            <td style='width: 70%;'>
                                                                <sub>" . $data['bidang_petugas'] . "</sub> <br>
                                                                " . $data['nama_petugas'] . "
                                                            </td>
                                                            <td>
                                                                <sub> </sub> <br>
                                                                " . $data['jam_kerja'] . "
                                                            </td>
                                                        </tr>";
                                                    }
                                                } else {
                                                    echo "0 results";
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- button jumat -->
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <a class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#jumat" aria-expanded="false" aria-controls="jumat">
                                        Jum'at
                                    </a>
                                </h5>
                            </div>
                            <!-- content Jum'at -->
                            <div id="jumat" class="collapse show" aria-labelledby="headingJumat" data-parent="#accordion">
                                <div class="card-body">
                                    <div class='table-responsive-sm'>
                                        <table class='table table-sm' style="width: 100%;">
                                            <tbody>
                                                <?php
                                                if (mysqli_num_rows($res_jumat) > 0) {
                                                    while ($data = mysqli_fetch_assoc($res_jumat)) {
                                                        echo "                                            
                                                        <tr>
                                                            <td style='width: 70%;'>
                                                                <sub>" . $data['bidang_petugas'] . "</sub> <br>
                                                                " . $data['nama_petugas'] . "
                                                            </td>
                                                            <td>
                                                                <sub> </sub> <br>
                                                                " . $data['jam_kerja'] . "
                                                            </td>
                                                        </tr>";
                                                    }
                                                } else {
                                                    echo "0 results";
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- button sabtu -->
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <a class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#sabtu" aria-expanded="false" aria-controls="sabtu">
                                        Sabtu
                                    </a>
                                </h5>
                            </div>
                            <!-- content Sabtu -->
                            <div id="sabtu" class="collapse show" aria-labelledby="headingSabtu" data-parent="#accordion">
                                <div class="card-body">
                                    <div class='table-responsive-sm'>
                                        <table class='table table-sm' style="width: 100%;">
                                            <tbody>
                                                <?php
                                                if (mysqli_num_rows($res_sabtu) > 0) {
                                                    while ($data = mysqli_fetch_assoc($res_sabtu)) {
                                                        echo "
                                                        <tr>
                                                            <td style='width: 70%;'>
                                                                <sub>" . $data['bidang_petugas'] . "</sub> <br>
                                                                " . $data['nama_petugas'] . "
                                                            </td>
                                                            <td>
                                                                <sub>  </sub> <br>
                                                                " . $data['jam_kerja'] . "
                                                            </td>
                                                        </tr>";
                                                    }
                                                } else {
                                                    echo "0 results";
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- konten jadwal selesai -->

                    </div>
                </div>
            </div>
        </div>
    </article>

    <!-- Footer-->
    <footer class="border-top">
        <div class="container px-4 px-lg-5 ">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="small text-center text-muted fst-italic">Copyright &copy; SPR Production 2021</div>
                    <div class="small text-center text-muted fst-italic">Powered by startbootstrap.com</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JS-->
    <script src="admin-side/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>