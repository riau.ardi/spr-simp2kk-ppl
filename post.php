<?php
include('admin-side/config.php');
$id = $_GET['id_page'];

$result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM berita WHERE nomor_berita = " . $id . " ;"));

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title> <?php echo $result['judul_berita'] ?> - SIMP2KK</title>
    <link rel="icon" type="image/x-icon" href="admin-side/assets/images/logo-small.png" />
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="css/styles.css" rel="stylesheet" />
</head>

<body>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
        <div class="container px-4 px-lg-5">
            <a class="navbar-brand" href="#"> </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto py-4 py-lg-0">
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="index.php">Home</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="daftar.php">Daftar Antrian</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="jadwal.php">Jadwal Praktik</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="about.php">Tentang Puskesmas</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Header-->
    <header style="height: 8vh; background-color: #6c757d;">

    </header>

    <!-- Post Content-->
    <article class="mb-4">
        <div class="container px-4 px-lg-5">
            <!-- post image -->
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class=" col-md-10 col-lg-8 col-xl-10">
                    <br>
                    <?php echo "<img class='img-fluid rounded' style='width:100%; object-fit: cover;' src='data:image/jpeg;base64," . base64_encode($result['img_data']) . "'/>" ?>
                    <div class="mt-3">
                        <h2><?php echo $result['judul_berita'] ?></h2>
                        <span>
                            Posted on <?php echo $result['tgl_berita'] ?>
                        </span>
                    </div>
                </div>

            </div>

            <!-- post text -->
            <div class=" row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-4 col-lg-8 col-xl-10">
                    <?php echo $result['isi_berita']; ?>
                </div>
            </div>
        </div>
    </article>

    <!-- Footer-->
    <footer class="border-top">
        <div class="container px-4 px-lg-5 ">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="small text-center text-muted fst-italic">Copyright &copy; SPR Production 2021</div>
                    <div class="small text-center text-muted fst-italic">Powered by startbootstrap.com</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JS-->
    <script src="admin-side/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>