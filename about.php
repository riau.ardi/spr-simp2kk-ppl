<?php include('admin-side/config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Tentang Puskesmas</title>
    <link rel="icon" type="image/x-icon" href="admin-side/assets/images/logo-small.png" />
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="admin-side/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link href="css/styles.css" rel="stylesheet" />
</head>

<body>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
        <div class="container px-4 px-lg-5">
            <a class="navbar-brand" href="#"> </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto py-4 py-lg-0">
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="index.php">Home</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="daftar.php">Daftar Antrian</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="jadwal.php">Jadwal Praktik</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="about.php">Tentang Puskesmas</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('admin-side/assets/images/puskesmas.jpg')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="page-heading">
                        <h3>Tentang Puskesmas</h3>
                        <span class="subheading"></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main Content-->
    <main class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-10">
                    <div>
                        <p style="text-align: justify;">
                            Puskesmas II Karang Klesem merupakan pusat pelayanan kesehatan masyarakat yang berperan
                            untuk memberikan layanan secara terpadu dan menyeluruh kepada masyarakat di wilayah
                            kecamatan Karang Klesem, juga berperan untuk melakukan pembinaan masyarakat dalam bidang
                            kesehatan.
                        </p>
                        <h3>VISI</h3>
                        <p style="text-align: justify;">
                            "Menjadi Pusat Kesehatan Masyarakat sebagai Pondasi Menuju Masyarakat Sehat yang Mandiri" <br>
                            Visi tersebut mengacu pada Visi Dinas Kesehatan Kabupaten Banyumas, yaitu: "Banyumas Sehat
                            dan Mandiri".
                        </p>
                        <h3>MISI</h3>
                        <p>
                        <ol>
                            <li>Meningkatkan kualitas manajemen pelayanan kesehatan.</li>
                            <li>Meningkatkan kualitas sarana dan prasarana pelayanan kesehatan.</li>
                            <li>Meningkatkan Ketrampilan dan Profesinalisme Tenaga Kesehatan.</li>
                        </ol>
                        </p>

                        <h3>Kontak Kami</h3>
                        <table style="width: 100%;">
                            <tr>
                                <td>Telp</td>
                                <td style="padding: 0px 5px;">:</td>
                                <td>0511-0897-011</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td style="padding: 0px 5px;">:</td>
                                <td>puskesklesem@gmail.co.id</td>
                            </tr>
                        </table>

                    </div>
                    <p style="text-align: center;">
                        <button class="btn btn-primary btn-lg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Hubungi Kami
                        </button>
                    </p>
                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <form id="contact-us" method="POST">
                                <div class="form-floating">
                                    <input class="form-control" id="name" name="name" type="text" placeholder="-" required />
                                    <label for="name">Nama</label>
                                </div>
                                <div class="form-floating">
                                    <input class="form-control" id="email" name="email" type="email" placeholder="-" required />
                                    <label for="email">Alamat Email</label>
                                </div>
                                <div class="form-floating">
                                    <input class="form-control" id="telpon" name="telpon" type="tel" placeholder="-" required />
                                    <label for="phone">Nomor Telepon</label>
                                </div>
                                <div class="form-floating">
                                    <textarea class="form-control" id="message" name="pesan" style="height: 12rem" placeholder="-" required></textarea>
                                    <label for="message">Pesan</label>
                                </div>
                                <br />
                                <p style="text-align: center;">
                                    <button class="btn btn-primary text-uppercase" id="submitButton" type="submit" name="submit">
                                        Kirim
                                    </button>
                                </p>
                            </form>
                            <?php
                            if (isset($_POST['submit'])) {
                                $nama = $_POST['name'];
                                $email = $_POST['email'];
                                $tel = $_POST['telpon'];
                                $pesan = $_POST['pesan'];

                                mysqli_query($koneksi, "INSERT INTO masukan (nama_masukan, email_masukan, telp_masukan, pesan_masukan) 
                                    VALUES ('" . $nama . "', '" . $email . "', '" . $tel . "', '" . $pesan . "');")
                                    or die(mysqli_error($koneksi));

                                echo ("<script>alert('Terima kasih atas masukan anda')</script>");
                                echo ("<meta http-equiv='refresh' content='0'>");
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Footer-->
    <footer class="border-top">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="small text-center text-muted fst-italic">Copyright &copy; SPR Production 2021</div>
                    <div class="small text-center text-muted fst-italic">Powered by startbootstrap.com</div>
                </div>
            </div>
        </div>
    </footer>

    <script src="admin-side/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/scripts.js"></script>
</body>

</html>