<?php
include('admin-side/config.php');
$tgl_now = date('Y-m-d');
$noCheck = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT IFNULL(max(no_antrian), 0)+1 as nomor from antrian where tgl_masuk = '" . $tgl_now . "';"));
$noCheckInsert = $noCheck['nomor'];
$antriSelesai = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT COUNT(no_antrian) as selesai from antrian where tgl_masuk = '" . $tgl_now . "' AND status_antrian = 1;")) or die(mysqli_error($koneksi));
$antriTunggu = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT COUNT(no_antrian) as tunggu from antrian where tgl_masuk = '" . $tgl_now . "';")) or die(mysqli_error($koneksi));

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Pendaftaran Antrian Puskesmas</title>
    <link rel="icon" type="image/x-icon" href="admin-side/assets/images/logo-small.png" />
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="admin-side/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link href="css/styles.css" rel="stylesheet" />

    <style>
        td {
            padding: 5px 5px;
        }

        input {
            border-top-style: hidden;
            border-right-style: hidden;
            border-left-style: hidden;
            border-bottom-style: groove;
        }
    </style>
</head>

<body>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
        <div class="container px-4 px-lg-5">
            <a class="navbar-brand" href="#"> </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto py-4 py-lg-0">
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="index.php">Home</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="daftar.php">Daftar Antrian</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="jadwal.php">Jadwal Praktik</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="about.php">Tentang Puskesmas</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Header-->
    <header style="height: 8vh; background-color: #6c757d;">

    </header>
    <!-- Main Content-->
    <main class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <p></p>

                    <!-- card jumlah antrian -->
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-check me-1"></i>
                                    Antrian Selesai
                                </div>
                                <div class="card-body j">
                                    <h1 style="text-align: center;">
                                        <?php echo $antriSelesai['selesai'] ?>
                                    </h1>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-clock me-1"></i>
                                    Total Antrian
                                </div>
                                <div class="card-body">
                                    <h1 style="text-align: center;">
                                        <?php echo $antriTunggu['tunggu'] ?>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card card-body">
                        <!-- form body -->
                        <div style="text-align: center;">
                            <p>
                            <h3> Pendaftaran Antrian</h3>
                            <sup>Tanggal : <?php echo $tgl_now; ?> </sup>
                            </p>
                        </div>

                        <form id="form-pendaftaran" method="POST" target="_blank" action="tiket.php?">
                            <table style="width: 100%;">
                                <tr>
                                    <td> <label for="nama">Nama</label> </td>
                                    <td> <input name="nama" id="nama" type="text" style="width: 100%;" placeholder="Masukkan nama anda..." required /> </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="jenisKelamin">Jenis Kelamin</label>
                                    </td>
                                    <td>
                                        <select name="jenisKelamin" id="jenisKelamin" required>
                                            <option value="Laki - Laki">Laki - Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td> <label for="goldar">Golongan Darah </label> </td>
                                    <td>
                                        <select name="goldar" id="goldar" required>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="AB">AB</option>
                                            <option value="O">O</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td> <label for="usia">Usia</label> </td>
                                    <td> <input name="usia" id="usia" type="text" style="width: 100%;" placeholder="Masukkan usia anda..." required /> </td>
                                </tr>
                                <tr>
                                    <td> <label for="keluhan">Keluhan</label> </td>
                                    <td> <input name="keluhan" id="keluhan" type="text" style="width: 100%;" placeholder="Masukkan keluhan anda..." required /> </td>
                                </tr>
                                <tr>
                                    <td> <label for="telepon">No. Telp</label> </td>
                                    <td> <input name="telepon" id="telepon" type="text" style="width: 100%;" placeholder="+628xx/ 08xx.." required /> </td>
                                </tr>
                            </table>
                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary btn-lg text-uppercase" id="submit" type="submit" name="register"> Daftar Antrian</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Footer-->
    <footer class="border-top">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="small text-center text-muted fst-italic">Copyright &copy; SPR Production 2021</div>
                    <div class="small text-center text-muted fst-italic">Powered by startbootstrap.com</div>
                </div>
            </div>
        </div>
    </footer>

    <script src="admin-side/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/scripts.js"></script>
</body>

</html>