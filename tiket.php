<?php
include_once 'admin-side/config.php';

if (isset($_POST['register'])) {
    $nama = $_POST['nama'];
    $jk = $_POST['jenisKelamin'];
    $goldar = $_POST['goldar'];
    $usia = $_POST['usia'];
    $keluhan = $_POST['keluhan'];
    $telepon = $_POST['telepon'];
    $tgl_regis = date('Y-m-d');


    $noCheck = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT IFNULL(max(no_antrian), 0)+1 as nomor from antrian where tgl_masuk = '" . $tgl_regis . "';"));
    $noCheckInsert = $noCheck['nomor'];

    $sqlQuery = "INSERT INTO antrian (no_antrian, nama_pengunjung, jenis_kelamin, usia, goldar, keluhan, no_telp, tgl_masuk) 
                            VALUES ('$noCheckInsert', '$nama', '$jk','$usia','$goldar','$keluhan', '$telepon', '$tgl_regis');";

    if (mysqli_query($koneksi, $sqlQuery)) {
        echo "
        <script type='text/javascript'>
        alert('Pendaftaran Berhasil');
        </script>";
    } else {
        echo "Error: " . $sqlQuery . " : " . mysqli_error($koneksi);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Detail Antrian</title>
    <link href="admin-side/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link rel="shortcut icon" href="admin-side/assets/images/logo-small.png" type="image/x-icon">
    <style>
        body {
            background-color: darkblue;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center
        }

        hr.new {
            border-top: 5px dashed #fff;
            margin: 0.4rem 0;
        }

        .btn-primary {
            color: #fff;
            background-color: #004cb9;
            border-color: #004cb9;
            padding: 12px;
            padding-right: 30px;
            padding-left: 30px;
            border-radius: 1px;
            font-size: 17px
        }

        .btn-primary:hover {
            color: #fff;
            background-color: #004cb9;
            border-color: #004cb9;
            padding: 12px;
            padding-right: 30px;
            padding-left: 30px;
            border-radius: 1px;
            font-size: 17px
        }

        .dot {
            height: 50px;
            width: 50px;
            background-color: darkblue;
            border-radius: 50%;
        }

        body {
            font-family: serif;
        }
    </style>
</head>

<body>
    <div class="container" style="width: 60vh;">
        <!-- bagian atas -->
        <div class="row d-flex justify-content-between" style="height: 1%;">
            <div class="dot" style=" transform: translate(0px, -40%);"></div>
            <div class="dot" style=" transform: translate(0px, -40%);"></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-l-4 p-4 " style=" width: 90%; height: 15vh; background-color: white;">
                <div class="text-center" style="transform: translate(0px, 30px);">
                    <h4> Puskesmas II <img src="admin-side/assets/images/logo-blue.png" style="height: auto; width: 40px; transform: translate(0px, -4px);"> Karang Klesem </h4>
                    <p style="font-size: 20px;">
                        Terima kasih sudah mendaftar <br>
                        Segera datangi Puskesmas
                    </p>
                </div>
            </div>
        </div>
        <!-- bagian bawah -->
        <div class="row justify-content-between" style="height: 0px;">
            <div class="col-4 dot" style=" transform: translate(0px, 0px);"></div>
            <div class="col-4 dot" style=" transform: translate(0px, 0px);"></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-l-4 p-4 " style=" width: 90%; height: 40vh; background-color: white;">
                <div class="table-responsive" style="margin-top: 60px;">
                    <table class="table" style="font-size: 20px; width: 100%;">
                        <tr>
                            <td>No</td>
                            <td>:</td>
                            <td><?php echo $noCheckInsert ?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><?php echo $nama ?></td>
                        </tr>
                        <tr>
                            <td>Keluhan</td>
                            <td>:</td>
                            <td><?php echo $keluhan ?></td>
                        </tr>
                        <tr>
                            <td>Gol. Darah</td>
                            <td>:</td>
                            <td><?php echo $goldar ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td><?php echo $tgl_regis ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-between">
            <div class="dot" style=" transform: translate(0px, -30px);"></div>
            <div class="dot" style=" transform: translate(0px, -30px);"></div>
        </div>
    </div>

    <script src="admin-side/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="admin-side/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="admin-side/node_modules/@popperjs/core/dist/umd/popper.js"></script>
    <script type="text/javascript">
        window.onload = function() {
            window.print();
        }
    </script>
</body>

</html>