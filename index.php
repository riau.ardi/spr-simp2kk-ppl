<?php
include('admin-side/config.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Puskesmas II Karang Klesem</title>
    <link rel="icon" type="image/x-icon" href="admin-side/assets/images/logo-small.png" />
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="css/styles.css" rel="stylesheet" />

</head>

<body>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
        <div class="container px-4 px-lg-5">
            <a class="navbar-brand" href="index.html"> </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto py-4 py-lg-0">
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="index.php">Home</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="daftar.php">Daftar Antrian</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="jadwal.php">Jadwal Praktik</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="about.php">Tentang Puskesmas</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('admin-side/assets/images/puskesmas.jpg')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <p class="subheading">Selamat Datang di Website</p>
                        <h2>Puskesmas II Karang Klesem</h2>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main Content-->
    <style>
        .wrapText {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
        }
    </style>
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-10">

                <!-- Post preview-->
                <?php
                $jumlahData = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM berita;"));
                $jumlahPage = ceil($jumlahData / 5);

                $limit = (isset($_GET['page'])) ? $_GET['page'] : 1;

                $sql = mysqli_query($koneksi, "SELECT * FROM berita ORDER BY nomor_berita DESC LIMIT " . ($limit * 5) - 5 . ", 5 ") or die(mysqli_error($koneksi));
                if (mysqli_num_rows($sql) > 0) {

                    while ($data = mysqli_fetch_assoc($sql)) {
                        echo "
                        <div class='post-preview'>                            
                            <img class='img-fluid rounded' style='width:100%; height:400px; object-fit: cover;' src='data:image/jpeg;base64," . base64_encode($data['img_data']) . "'/>
                            <a href='post.php?id_page=" . $data['nomor_berita'] . "'>
                                <h2 class='post-title'>" . $data['judul_berita'] . "</h2>
                                <h3 class='post-subtitle wrapText'>" . $data['isi_berita'] . "</h3>
                            </a>
                            <p class='post-meta'>
                                Posted on " . $data['tgl_berita'] . "
                            </p>
                        </div>
                        <!-- Divider-->
                        <hr class=4my-4' />";
                    }
                } else {
                    echo "0 results";
                }
                ?>
                <!-- Pager-->
                <form action="" method="POST">
                    <div class="d-flex justify-content-between mb-4" style="width: 100%;">
                        <a class="btn btn-primary text-uppercase" id="next" href="index.php?page=<?php echo $limit - 1 ?>">← Berita sebelumnya</a>
                        <a class="btn btn-primary text-uppercase" id="prev" href="index.php?page=<?php echo $limit + 1 ?>">Berita selanjutnya →</a>
                    </div>
                </form>
                <script>
                    var next = document.getElementById("prev");
                    var prev = document.getElementById("next");
                    if (<?php echo $limit ?> == 1) {
                        prev.classList.add("disabled");
                    } else if (<?php echo $limit ?> == <?php echo $jumlahPage ?>) {
                        next.classList.add("disabled");
                    }
                </script>
                <?php
                if ($limit > 1) {
                    if ($_GET['page']) {
                        $limit = $_GET['page'];
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!-- Footer-->
    <footer class="border-top">
        <div class="container px-4 px-lg-5 ">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="small text-center text-muted fst-italic">Copyright &copy; SPR Production 2021</div>
                    <div class="small text-center text-muted fst-italic">Powered by startbootstrap.com</div>
                </div>
            </div>
        </div>
    </footer>

    <script src="admin-side/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/scripts.js"></script>


</body>

</html>