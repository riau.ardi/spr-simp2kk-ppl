<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('config.php');
$tgl_now = date('Y-m-d');
$tanggal_pasien = '';
$jumlah_pasien = '';
$tanggal_antrian = '';
$jumlah_antrian = '';

// memanggil daftar antrian ke table
$res_antri = mysqli_query($koneksi, "SELECT * from antrian where tgl_masuk = '" . $tgl_now . "' AND status_antrian = 0;") or die(mysqli_error($koneksi));
// memanggil jumlah antrian yang selesai hari ini
$antriSelesai = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT COUNT(no_antrian) as selesai from antrian where tgl_masuk = '" . $tgl_now . "' AND status_antrian = 1;")) or die(mysqli_error($koneksi));
// memanggil jumlah antrian hari ini
$antriTunggu = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT COUNT(no_antrian) as tunggu from antrian where tgl_masuk = '" . $tgl_now . "';")) or die(mysqli_error($koneksi));

$limit = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT IF(COUNT(DISTINCT tgl_masuk) <= 7, 7, COUNT(DISTINCT tgl_masuk)) as maksimal FROM antrian;"));
$limit_num = $limit['maksimal'] - 7;

// koneksi table pasien ke chart
$chart_pasien = mysqli_query($koneksi, "SELECT tanggal_masuk AS Tanggal, COUNT('nomor') AS Jumlah FROM pasien GROUP BY tanggal_masuk limit " . $limit_num . ", 7;") or die(mysqli_error($koneksi));
while ($row = mysqli_fetch_array($chart_pasien)) {
    $tanggal_pasien = $tanggal_pasien . '"' . $row['Tanggal'] . '",';
    $jumlah_pasien = $jumlah_pasien . '"' . $row['Jumlah'] . '",';
}
$tanggal_pasien = trim($tanggal_pasien, ",");
$jumlah_pasien = trim($jumlah_pasien, ",");

// koneksi table antrian ke chart
$chart_antrian = mysqli_query($koneksi, "SELECT tgl_masuk AS Tanggal, COUNT('nomor antrian') AS Jumlah FROM antrian GROUP BY tgl_masuk limit " . $limit_num . ", 7") or die(mysqli_error($koneksi));
while ($row = mysqli_fetch_array($chart_antrian)) {
    $tanggal_antrian = $tanggal_antrian . '"' . $row['Tanggal'] . '",';
    $jumlah_antrian = $jumlah_antrian . '"' . $row['Jumlah'] . '",';
}
$tanggal_antrian = trim($tanggal_antrian, ",");
$jumlah_antrian = trim($jumlah_antrian, ",");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <link href="assets/script/css/styles.css" rel="stylesheet" />
    <link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">


</head>

<body>
    <div class="container-fluid px-4">
        <!-- title page -->
        <p>
        <h1 style="text-align: center;">Sistem Informasi Manajemen Puskesmas II Karang Klesem</h1>
        <hr>
        </p>

        <!-- konten website -->
        <div class="row">
            <div class="col-xl-2">
                <div class="row-xl-1">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-check me-1"></i>
                            Antrian Selesai
                        </div>
                        <div class="card-body j">
                            <h1 style="text-align: center;">
                                <?php echo $antriSelesai['selesai'] ?>
                            </h1>
                        </div>
                    </div>
                </div>

                <div class="row-xl-1">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-clock me-1"></i>
                            Total Antrian
                        </div>
                        <div class="card-body">
                            <h1 style="text-align: center;">
                                <?php echo $antriTunggu['tunggu'] ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-chart-area me-1"></i>
                        Jumlah Kunjungan
                    </div>
                    <div class="card-body">
                        <canvas id="chart-data" style=" width: 100%; height: 13.2em; min-height: 13.2em;"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <!-- daftar antrian -->
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Sisa Antrian : <?php echo $tgl_now ?>
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Gol. Darah</th>
                            <th>Keluhan</th>
                            <th>Nomor Telepon</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nomor</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Gol. Darah</th>
                            <th>Keluhan</th>
                            <th>Nomor Telepon</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php
                        if (isset($_POST['selesai'])) {
                            $id = $_POST['selesai'];
                            echo ("<meta http-equiv='refresh' content='0'>");
                            mysqli_query($koneksi, "UPDATE antrian SET status_antrian = '1' WHERE id_antrian = " . $id . ";");
                        }
                        if (isset($_POST['rawat'])) {
                            $id = $_POST['rawat'];
                            $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM antrian WHERE id_antrian = " . $id . " ;"));
                            $add_nama = $result['nama_pengunjung'];
                            $add_jk = $result['jenis_kelamin'];
                            $add_goldar = $result['goldar'];
                            $add_usia = $result['usia'];
                            $add_keluhan = $result['keluhan'];
                            $add_tgl_masuk = $result['tgl_masuk'];
                            $add_telp = $result['no_telp'];
                            mysqli_query($koneksi, "UPDATE antrian SET status_antrian = '1' WHERE id_antrian = " . $id . ";");
                            mysqli_query($koneksi, "INSERT INTO pasien (nama_pasien, jenis_kelamin, goldar, usia, keluhan, tanggal_masuk, no_telp)
                                                    VALUES ('" . $add_nama . "', '" . $add_jk . "','" . $add_goldar . "','" . $add_usia . "','" . $add_keluhan . "','" . $add_tgl_masuk . "','" . $add_telp . "');") or die(mysqli_error($koneksi));

                            echo ("<meta http-equiv='refresh' content='0'>");
                            echo "<script>alert('Antrian ID : " . $id . " dengan nama " . $add_nama . " ditambahkan sebagai pasien')</script>";
                        }


                        if (mysqli_num_rows($res_antri) > 0) {
                            while ($data = mysqli_fetch_assoc($res_antri)) {
                                echo "
                                <tr>                                                                    
                                    <td>" . $data['no_antrian'] . "</td>
                                    <td>" . $data['nama_pengunjung'] . "</td>
                                    <td>" . $data['jenis_kelamin'] . "</td>
                                    <td>" . $data['usia'] . "</td>
                                    <td>" . $data['goldar'] . "</td>                                
                                    <td>" . $data['keluhan'] . "</td>
                                    <td>" . $data['no_telp'] . "</td>
                                    <td>                                                                        
                                        
                                        <form method='post'>
                                            <button class='btn btn-primary' type='submit' name='selesai' value='" . $data['id_antrian'] . "'>Selesai</button>
                                            <button class='btn btn-secondary' type='submit' name='rawat' value='" . $data['id_antrian'] . "'>Rawat Inap</button>
                                        </form>                                        
                                    </td>
                                </tr>
                                ";
                            }
                        } else {
                            echo "0 results";
                        } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="assets/script/js/datatables-simple-demo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" crossorigin="anonymous"></script>
    <script>
        // draw database ke chart pasien
        var ctx = document.getElementById("chart-data").getContext('2d');
        var ChartPasien = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [<?php echo $tanggal_antrian ?>],
                datasets: [{
                        label: 'Jumlah Pasien Masuk',
                        lineTension: 0.3,
                        backgroundColor: "rgba(2,117,216,0.2)",
                        borderColor: "rgba(2,117,216,1)",
                        pointRadius: 5,
                        pointBackgroundColor: "rgba(2,117,216,1)",
                        pointBorderColor: "rgba(255,255,255,0.8)",
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(2,117,216,1)",
                        pointHitRadius: 50,
                        pointBorderWidth: 2,
                        data: [<?php echo $jumlah_pasien ?>]
                    },
                    {
                        label: 'Jumlah Antrian Masuk',
                        lineTension: 0.3,
                        backgroundColor: "rgba(2,117,216,0.2)",
                        borderColor: "rgba(140,17,246,1)",
                        pointRadius: 5,
                        pointBackgroundColor: "rgba(140,17,246,1)",
                        pointBorderColor: "rgba(255,255,255,0.8)",
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(2,117,216,1)",
                        pointHitRadius: 50,
                        pointBorderWidth: 2,
                        data: [<?php echo $jumlah_antrian ?>]
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'date'
                        },
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            maxTicksLimit: 7
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 20,
                            maxTicksLimit: 5
                        },
                        gridLines: {
                            color: "rgba(0, 0, 0, .125)",
                        }
                    }],
                },
                legend: {
                    display: false
                }
            }
        });
    </script>
</body>

</html>