<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
$res_pasien = mysqli_query($koneksi, "SELECT * from pasien") or die(mysqli_error($koneksi));
$tanggal_pasien = "";
$jumlah_pasien = "";

$limit = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT IF(COUNT(DISTINCT tanggal_masuk) <= 7, 7, COUNT(DISTINCT tanggal_masuk)) as maksimal FROM pasien;"));
$limit_num = $limit['maksimal'] - 7;
$chart_antrian = mysqli_query($koneksi, "SELECT tanggal_masuk AS Tanggal, COUNT('nomor') AS Jumlah FROM pasien GROUP BY tanggal_masuk limit " . $limit_num . ", 7;") or die(mysqli_error($koneksi));
while ($row = mysqli_fetch_array($chart_antrian)) {
    $tanggal_pasien = $tanggal_pasien . '"' . $row['Tanggal'] . '",';
    $jumlah_pasien = $jumlah_pasien . '"' . $row['Jumlah'] . '",';
}
$tanggal_pasien = trim($tanggal_pasien, ",");
$jumlah_pasien = trim($jumlah_pasien, ",");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid px-4">
        <!-- title page -->
        <p>
        <h1 style="text-align: center;">Daftar Data Pasien</h1>
        <hr>
        </p>

        <!-- konten website -->

        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-chart-area me-1"></i>
                Pertambahan Pasien 7 Hari Terakhir
            </div>
            <div class="card-body"><canvas id="chart-pasien" style="width: 100%; height: auto;"></canvas></div>
        </div>


        <!-- daftar pasien -->
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Tabel Data Pasien
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Gol. Darah</th>
                            <th>Keluhan</th>
                            <th>No. Telp</th>
                            <th>Ruangan</th>
                            <th>Dokter yang menangani</th>
                            <th>Tanggal Masuk</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nomor</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Gol. Darah</th>
                            <th>Keluhan</th>
                            <th>No. Telp</th>
                            <th>Ruangan</th>
                            <th>Dokter yang menangani</th>
                            <th>Tanggal Masuk</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php
                        if (mysqli_num_rows($res_pasien) > 0) {
                            while ($data = mysqli_fetch_assoc($res_pasien)) {
                                echo "
                                <tr>                                                                                                         
                                    <td>" . $data['nomor'] . "</td>                                        
                                    <td>" . $data['nama_pasien'] . "</td>
                                    <td>" . $data['jenis_kelamin'] . "</td>
                                    <td>" . $data['usia'] . "</td>
                                    <td>" . $data['goldar'] . "</td>                                
                                    <td>" . $data['keluhan'] . "</td>                                
                                    <td>" . $data['no_telp'] . "</td>
                                    <td>" . $data['ruang_pasien'] . "</td>
                                    <td>" . $data['dokter_pasien'] . "</td>
                                    <td>" . $data['tanggal_masuk'] . "</td>
                                    <td> 
                                        <form method='post'>
                                            <a class='btn btn-primary' style='width: 80px;' name='edit' href='edit_pasien.php?id_pasien=" . $data['nomor'] . "' >Edit</a>
                                            <button class='btn btn-danger' style='width: 80px;' method='post' type='submit' name='delete' value='" . $data['nomor'] . "'>Hapus</button>                                        
                                        </form>  
                                    </td>                                    
                                </tr>
                                ";
                            }
                        } else {
                            echo "0 results";
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php

    if (isset($_POST['delete'])) {
        $id = $_POST['delete'];
        mysqli_query($koneksi, "DELETE FROM pasien WHERE nomor =  " . $id . ";");
        echo ("<meta http-equiv='refresh' content='0'>");
        echo "<script>alert('Data : " . $id . " dihapus')</script>";
    }
    ?>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>
    <script>
        // draw database ke chart antrian
        var ctx = document.getElementById("chart-pasien").getContext('2d');
        var chartPasien = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [<?php echo $tanggal_pasien ?>],
                datasets: [{
                    label: 'Jumlah Pertambahan Pasien',
                    lineTension: 0.3,
                    backgroundColor: "rgba(2,117,216,0.2)",
                    borderColor: "rgba(2,117,216,1)",
                    pointRadius: 5,
                    pointBackgroundColor: "rgba(2,117,216,1)",
                    pointBorderColor: "rgba(255,255,255,0.8)",
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(2,117,216,1)",
                    pointHitRadius: 50,
                    pointBorderWidth: 2,
                    data: [<?php echo $jumlah_pasien ?>],
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'date'
                        },
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            maxTicksLimit: 7
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 20,
                            maxTicksLimit: 5
                        },
                        gridLines: {
                            color: "rgba(0, 0, 0, .125)",
                        }
                    }],
                },
                legend: {
                    display: false
                }
            }
        });
    </script>
</body>

</html>