<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
// nilai maksimal Id pasien
$result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(nomor) FROM pasien;"));
$maks_id = $result['MAX(nomor)'] + 1;

//fecth dari antrian
$res_antri = mysqli_query($koneksi, "SELECT * from antrian WHERE status_antrian = 0") or die(mysqli_error($koneksi));

$in_rng = mysqli_query($koneksi, "SELECT nama_ruang FROM ruangan;") or die(mysqli_error($koneksi));
$in_dok = mysqli_query($koneksi, "SELECT nama_petugas FROM petugas;") or die(mysqli_error($koneksi));
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-2 col-sm-10"></div>
            <div class="col-xl-10 col-sm-10">
                <h1 class="mt-4">Data antrian</h1>
                <ol class="breadcrumb mb-4 ">
                    <li class="breadcrumb-item"><a href="lihat_pasien.php">Daftar Pasien</a></li>
                    <li class="breadcrumb-item active">Pasien ID : <?php echo $maks_id ?></li>
                </ol>
            </div>

            <style>
                input {
                    width: 80%;
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }

                td {
                    padding: 0px 10px;
                }
            </style>
            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data pasien baru
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form id="form-pendaftaran" method="post">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 20%;">
                                        <p> <label for="nama">Nama</label></p>
                                    </td>
                                    <td>
                                        <p> <input name="nama" id="nama" type="text" value="" required /> </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="jenisKelamin">Jenis Kelamin</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="jenisKelamin" id="jenisKelamin" required>
                                                <option value="Laki - Laki">Laki - Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="goldar">Golongan Darah </label> </p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="goldar" id="goldar" required>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="AB">AB</option>
                                                <option value="O">O</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p> <label for="usia">Usia</label> </p>
                                    </td>
                                    <td>
                                        <p><input name="usia" id="usia" type="number" style="width: auto;" value="" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="keluhan">Keluhan</label></p>
                                    </td>
                                    <td>
                                        <p><input name="keluhan" id="keluhan" type="text" value="" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="telepon">No. Telp</label></p>
                                    </td>
                                    <td>
                                        <p><input name="telepon" id="telepon" type="text" value="" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="ruang">Ruangan</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="ruang" id="ruang" required>
                                                <?php
                                                if (mysqli_num_rows($in_rng) > 0) {
                                                    while ($data = mysqli_fetch_assoc($in_rng)) {
                                                        echo "<option value='" . $data['nama_ruang'] . "'>" . $data['nama_ruang'] . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>

                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="dokter">Dokter yang menangani</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="dokter" id="dokter" required>
                                                <?php
                                                if (mysqli_num_rows($in_dok) > 0) {
                                                    while ($data = mysqli_fetch_assoc($in_dok)) {
                                                        echo "<option value='" . $data['nama_petugas'] . "'>" . $data['nama_petugas'] . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="tgl_masuk">Tanggal Masuk</label></p>
                                    </td>
                                    <td>
                                        <p><input name="tgl_masuk" id="tgl_masuk" type="date" style="width: auto;" value="" required /></p>
                                    </td>
                                </tr>

                            </table>
                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="submit" type="submit" name="tambah_pasien">
                                    Tambah Data
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_pasien.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['tambah_pasien'])) {
                        $name = $_POST['nama'];
                        $jk = $_POST['jenisKelamin'];
                        $goldar = $_POST['goldar'];
                        $usia = $_POST['usia'];
                        $keluhan = $_POST['keluhan'];
                        $no_telp = $_POST['telepon'];
                        $ruang_pas = $_POST['ruang'];
                        $dokter_pas = $_POST['dokter'];
                        $tgl_msk = $_POST['tgl_masuk'];

                        mysqli_query($koneksi, "INSERT INTO pasien (nama_pasien, jenis_kelamin, goldar, usia, keluhan, no_telp, ruang_pasien, dokter_pasien, tanggal_masuk)
                        VALUES ('" . $name . "', '" . $jk . "','" . $goldar . "','" . $usia . "','" . $keluhan . "','" . $no_telp . "','" . $ruang_pas . "','" . $dokter_pas . "','" . $tgl_msk . "');") or die(mysqli_error($koneksi));

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Data " . $name . " Ditambahkan')</script>");
                    }

                    ?>
                </div>
            </div>

            <!-- cari dari data antrian -->
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Tambah data dari tabel antrian
                </div>
                <div class="card-body">
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Usia</th>
                                <th>Gol. Darah</th>
                                <th>Keluhan</th>
                                <th>Tanggal Masuk</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Usia</th>
                                <th>Gol. Darah</th>
                                <th>Keluhan</th>
                                <th>Tanggal Masuk</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>

                        <tbody>
                            <?php
                            $count = 0;
                            if (mysqli_num_rows($res_antri) > 0) {
                                while ($data = mysqli_fetch_assoc($res_antri)) {

                                    echo "
                                <tr>                                                                    
                                    <td>" . $data['id_antrian'] . "</td>
                                    <td>" . $data['nama_pengunjung'] . "</td>
                                    <td>" . $data['jenis_kelamin'] . "</td>
                                    <td>" . $data['usia'] . "</td>
                                    <td>" . $data['goldar'] . "</td>                                
                                    <td>" . $data['keluhan'] . "</td>
                                    <td>" . $data['tgl_masuk'] . "</td>
                                    <td> 
                                        <form method='post'>                                            
                                            <button class='btn btn-primary' method='post' type='submit' name='tambah' value='" . $data['id_antrian'] . "'>Tambah</button>                                        
                                        </form>  
                                    </td>
                                    
                                </tr>
                                ";
                                }
                            } else {
                                echo "0 results";
                            } ?>

                        </tbody>
                    </table>
                    <?php

                    if (isset($_POST['tambah'])) {
                        $id = $_POST['tambah'];
                        $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM antrian WHERE id_antrian = " . $id . " ;"));
                        $add_nama = $result['nama_pengunjung'];
                        $add_jk = $result['jenis_kelamin'];
                        $add_goldar = $result['goldar'];
                        $add_usia = $result['usia'];
                        $add_keluhan = $result['keluhan'];
                        $add_tgl_masuk = $result['tgl_masuk'];

                        mysqli_query($koneksi, "INSERT INTO pasien (nama_pasien, jenis_kelamin, goldar, usia, keluhan, tanggal_masuk)
                        VALUES ('" . $add_nama . "', '" . $add_jk . "','" . $add_goldar . "','" . $add_usia . "','" . $add_keluhan . "','" . $add_tgl_masuk . "');") or die(mysqli_error($koneksi));

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo "<script>alert('Antrian ID : " . $id . " dengan nama " . $add_nama . " ditambahkan sebagai pasien')</script>";
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>

    <script type="text/javascript">
        function selectElement(id, valueToSelect) {}
    </script>

</body>

</html>