<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');

$id = $_GET['id_pasien'];
if ($id != 0) {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM pasien WHERE nomor = " . $id . " ;"));
} else {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(nomor) FROM pasien;"));
    $maks_id = $result['MAX(nomor)'];
    $result['nomor'] = $maks_id + 1;
    $result['nama_pasien'] = '';
    $result['jenis_kelamin'] = 'Laki - Laki';
    $result['goldar'] = 'A';
    $result['usia'] = '';
    $result['keluhan'] = '';
    $result['tanggal_masuk'] = date('Y-m-d');
}

$in_rng = mysqli_query($koneksi, "SELECT nama_ruang FROM ruangan;") or die(mysqli_error($koneksi));
$in_dok = mysqli_query($koneksi, "SELECT nama_petugas FROM petugas;") or die(mysqli_error($koneksi));

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-8 col-sm-10">
                <h1 class="mt-4">Data Pasien</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="lihat_pasien.php">Daftar Pasien</a></li>
                    <li class="breadcrumb-item active">Pasien ID : <?php echo $result['nomor'] ?></li>
                </ol>
            </div>
            <!-- style tabel -->
            <style>
                input {
                    width: 80%;
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }

                td {
                    padding: 0px 10px;
                }
            </style>

            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Pasien
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form id="form-pendaftaran" method="post">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 20%;">
                                        <p> <label for="nama">Nama</label></p>
                                    </td>
                                    <td style="width: 80%;">
                                        <p> <input name="nama" id="nama" type="text" value="<?php echo $result['nama_pasien'] ?>" required /> </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="jenisKelamin">Jenis Kelamin</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="jenisKelamin" id="jenisKelamin" required>
                                                <option value="Laki - Laki">Laki - Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="goldar">Golongan Darah </label> </p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="goldar" id="goldar" required>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="AB">AB</option>
                                                <option value="O">O</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p> <label for="usia">Usia</label> </p>
                                    </td>
                                    <td>
                                        <p><input name="usia" id="usia" type="number" value="<?php echo $result['usia'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="keluhan">Keluhan</label></p>
                                    </td>
                                    <td>
                                        <p><input name="keluhan" id="keluhan" type="text" value="<?php echo $result['keluhan'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="telepon">No. Telp</label></p>
                                    </td>
                                    <td>
                                        <p><input name="telepon" id="telepon" type="text" value="<?php echo $result['no_telp'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="ruang">Ruangan</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="ruang" id="ruang" required>
                                                <?php
                                                if (mysqli_num_rows($in_rng) > 0) {
                                                    while ($data = mysqli_fetch_assoc($in_rng)) {
                                                        echo "<option value='" . $data['nama_ruang'] . "'>" . $data['nama_ruang'] . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>

                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="dokter">Dokter yang menangani</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="dokter" id="dokter" required>
                                                <?php
                                                if (mysqli_num_rows($in_dok) > 0) {
                                                    while ($data = mysqli_fetch_assoc($in_dok)) {
                                                        echo "<option value='" . $data['nama_petugas'] . "'>" . $data['nama_petugas'] . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="telepon">Tanggal Masuk</label></p>
                                    </td>
                                    <td>
                                        <p><input name="tgl_masuk" id="tgl_masuk" type="date" style="width: auto;" value="<?php echo $result['tanggal_masuk'] ?>" required /></p>
                                    </td>
                                </tr>
                            </table>

                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="submit" type="submit" name="edit_petugas">
                                    Edit Antrian
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_pasien.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['edit_petugas'])) {
                        $name = $_POST['nama'];
                        $jk = $_POST['jenisKelamin'];
                        $goldar = $_POST['goldar'];
                        $usia = $_POST['usia'];
                        $keluhan = $_POST['keluhan'];
                        $no_telp = $_POST['telepon'];
                        $ruang_pas = $_POST['ruang'];
                        $dokter_pas = $_POST['dokter'];
                        $tgl_msk = $_POST['tgl_masuk'];

                        mysqli_query($koneksi, "UPDATE pasien SET nama = '" . $name . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET jenis_kelamin = '" . $jk . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET usia = '" . $usia . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET goldar = '" . $goldar . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET keluhan = '" . $keluhan . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET no_telp = '" . $no_telp . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET ruang_pasien = '" . $ruang_pas . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET dokter_pasien = '" . $dokter_pas . "' WHERE nomor = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET tanggal_masuk = '" . $tgl_msk . "' WHERE nomor = " . $id . ";");
                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Data Pasien " . $name . " Diupdate')</script>");
                    }

                    ?>
                </div>
            </div>
        </div>

    </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>

    <script type="text/javascript">
        function selectElement(id, valueToSelect) {
            let element = document.getElementById(id);
            element.value = valueToSelect;
        }

        selectElement('jenisKelamin', '<?php echo $result['jenis_kelamin'] ?>');
        selectElement('goldar', '<?php echo $result['goldar'] ?>');
        selectElement('ruang', '<?php echo $result['ruang_pasien'] ?>');
        selectElement('dokter', '<?php echo $result['dokter_pasien'] ?>');
    </script>

</body>

</html>