<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dummysimp2kk";

// Create connection
$koneksi = new mysqli($servername, $username, $password, $dbname);
// Check connection
if (mysqli_connect_errno()) {
    echo "Gagal melakukan koneksi ke MySQL:" . mysqli_connect_error();
}

$pdo_koneksi = new PDO("mysql:host=localhost;dbname=dummysimp2kk", "root", "");

function registrasi($data)
{
    global $koneksi;
    $username = strtolower(stripslashes($data['username']));
    $password = mysqli_real_escape_string($koneksi, $data['password']);
    $password2 = mysqli_real_escape_string($koneksi, $data['password-conf']);

    $result = mysqli_query($koneksi, "SELECT username FROM akun WHERE username = '" . $username . "';");

    if (mysqli_fetch_assoc($result)) {
        echo "<script>
            alert('Username sudah digunakan');
        </script>";

        return false;
    }

    if ($password !== $password2) {
        echo "<script>
            alert('Password tidak sesuai');
        </script>";
        return false;
    }

    $password = password_hash($password, PASSWORD_DEFAULT);
    mysqli_query($koneksi, "INSERT INTO akun VALUES('', '" . $username . "', '" . $password . "');");

    return mysqli_affected_rows($koneksi);
}

function mulai_sesi()
{
}
