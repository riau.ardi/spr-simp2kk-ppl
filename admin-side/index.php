<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Dashboard - SIMP2KK</title>
    <link rel="shortcut icon" href="assets/images/logo-small.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="assets/script/css/styles.css" rel="stylesheet" />
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <script>
        function loadDeferredIframe() {
            var mainframe = document.getElementById("main");
            mainframe.src = "home.php"
        };
        window.onload = loadDeferredIframe;
    </script>
</head>

<body class="sb-nav-fixed">
    <!-- Navigation Start -->
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Name-->
        <a class="navbar-brand ps-3" id="heading">Dashboard - SIMP2KK</a>

        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle">
            <i class="fas fa-bars"></i>
        </button>

        <div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0"></div>
        <!-- User icon -->
        <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                </ul>
            </li>
        </ul>

    </nav>
    <!-- Navigation end -->

    <!-- sidebar menu -->
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <!-- sidebar Dashboard button -->
                        <div class="sb-sidenav-menu-heading">SIMP2KK</div>
                        <a class="nav-link" href="home.php" target="main">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div> Dashboard
                        </a>

                        <!-- button manajemen data -->
                        <div class="sb-sidenav-menu-heading">Manajemen Data</div>

                        <!-- data antrian -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-antrian" aria-expanded="false" aria-controls="data_antrian">
                            <div class="sb-nav-link-icon"><i class="fas fa-ticket-alt"></i></div>
                            Data Antrian
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-antrian" aria-labelledby="heading-antrian" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="db_antrian/lihat_antrian.php" target="main">Lihat Data</a>
                            </nav>
                        </div>

                        <!-- data pasien -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-pasien" aria-expanded="false" aria-controls="data-pasien">
                            <div class="sb-nav-link-icon"><i class="fas fa-hospital-user"></i></div>
                            Data Pasien
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-pasien" aria-labelledby="heading-pasien" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="db_pasien/lihat_pasien.php" target="main">Lihat Data</a>
                                <a class="nav-link" href="db_pasien/tambah_pasien.php" target="main">Tambah Data</a>
                            </nav>
                        </div>

                        <!-- data petugas -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-petugas" aria-expanded="false" aria-controls="data-petugas">
                            <div class="sb-nav-link-icon"><i class="fas fa-stethoscope"></i></div>
                            Data Petugas
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-petugas" aria-labelledby="heading-petugas" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="db_petugas/lihat_petugas.php" target="main">Lihat Data</a>
                                <a class="nav-link" href="db_petugas/tambah_petugas.php" target="main">Tambah Data</a>
                            </nav>
                        </div>

                        <!-- data ruangan -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-ruangan" aria-expanded="false" aria-controls="data-ruangan">
                            <div class="sb-nav-link-icon"><i class="fas fa-bed"></i></div>
                            Data Ruangan
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-ruangan" aria-labelledby="heading-ruangan" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="db_ruang/lihat_ruang.php" target="main">Lihat Data</a>
                            </nav>
                        </div>

                        <!-- button manajemen data -->
                        <div class="sb-sidenav-menu-heading">Manajemen Dokumen</div>

                        <!-- data berita -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-berita" aria-expanded="false" aria-controls="data-berita">
                            <div class="sb-nav-link-icon"><i class="fas fa-newspaper"></i></div>
                            Dokumen Berita
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-berita" aria-labelledby="heading-berita" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="db_berita/lihat_berita.php" target="main">Lihat Data</a>
                                <a class="nav-link" href="db_berita/tambah_berita.php" target="main">Tambah Data</a>
                            </nav>
                        </div>

                        <!-- data rekap -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-rekap" aria-expanded="false" aria-controls="data-rekap">
                            <div class="sb-nav-link-icon"><i class="fas fa-file-signature"></i></div>
                            Dokumen Puskesmas
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-rekap" aria-labelledby="heading-rekap" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="db_rekap/lihat_rekap.php" target="main">Lihat Data</a>
                                <a class="nav-link" href="db_rekap/tambah_rekap.php" target="main">Tambah Data</a>
                            </nav>
                        </div>

                        <!-- data masukan -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-masukan" aria-expanded="false" aria-controls="data-masukan">
                            <div class="sb-nav-link-icon"><i class="fas fa-comment-medical"></i></div>
                            Masukan Pengunjung
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-masukan" aria-labelledby="heading-masukan" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="masukan.php" target="main">Lihat Data</a>
                            </nav>
                        </div>

                        <!--Manajemen Akun-->
                        <div class="sb-sidenav-menu-heading">Manajemen Akun</div>
                        <!-- data akun -->
                        <a class="nav-link collapsed" data-bs-toggle="collapse" data-bs-target="#data-akun" aria-expanded="false" aria-controls="data-akun">
                            <div class="sb-nav-link-icon"><i class="fas fa-user-circle"></i></div>
                            Kelola Akun
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>

                        <div class="collapse" id="data-akun" aria-labelledby="heading-akun" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="akun.php" target="main">Lihat Data</a>
                            </nav>
                        </div>

                    </div>
                </div>

                <!-- footer sidebar -->
                <div class="sb-sidenav-footer">
                    <div class="small">Logged in as: <br> <?php echo $_SESSION['userlogin'] ?></div>

                </div>

            </nav>
        </div>

        <!-- konten website -->
        <div id="layoutSidenav_content">
            <iframe name="main" id="main" style="width: 100%; height: 100%; margin: 20px 0px;" src="">

            </iframe>


            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid px-4">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; SIMP2KK 2021</div>
                        <div class="text-muted">Powered by Start Bootstrap</div>
                    </div>
                </div>
            </footer>
        </div>

    </div>

    <script src="assets/script/js/scripts.js"></script>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>