<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('config.php');
if (isset($_POST['register'])) {
    if (registrasi($_POST) > 0) {
        echo "<script>
            alert('Akun baru ditambahkan')
        </script>";
    } else {
        echo mysqli_error($koneksi);
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <link href="assets/script/css/styles.css" rel="stylesheet" />
    <link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <div class="card-header text-center">
                        <h2>Daftar Akun yang terdaftar</h2>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-sm-10">
                                <div class="card p-2 mb-2">
                                    <form action="" method="POST">
                                        <h5>
                                            <b>Registrasi Akun Baru</b>
                                        </h5>
                                        <p>
                                            <label for="username">Username </label> <br>
                                            <input type="text" name="username" id="username">
                                        </p>
                                        <p>
                                            <label for="password">Password </label> <br>
                                            <input type="password" name="password" id="password">
                                        </p>
                                        <p>
                                            <label for="password-conf">Konfirmasi Password</label> <br>
                                            <input type="password" name="password-conf" id="password-conf">
                                        </p>
                                        <p>
                                            <button type="submit" class="btn btn-primary" name="register">Registrasi</button>
                                        </p>
                                    </form>
                                </div>

                            </div>

                            <div class="col-xl-8 col-sm-10">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Username</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Username</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        $res_akun = mysqli_query($koneksi, "SELECT * FROM akun;");
                                        if (mysqli_num_rows($res_akun) > 0) {
                                            while ($row = mysqli_fetch_assoc($res_akun)) {
                                        ?>
                                                <tr>
                                                    <td><?php echo $row['id'] ?></td>
                                                    <td><?php echo $row['username'] ?></td>
                                                    <td>
                                                        <form method='post'>
                                                            <button class='btn btn-danger' style='width: 80px;' method='post' type='submit' name='delete' value="<?php echo $row['id'] ?>">Hapus</button>
                                                            <input type='hidden' name='username' value=" <?php echo $row['username'] ?>">
                                                        </form>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }

                                        if (isset($_POST['delete'])) {
                                            $id = $_POST['delete'];
                                            $nama = $_POST['username'];
                                            mysqli_query($koneksi, "DELETE FROM akun WHERE id =  " . $id . ";");
                                            echo ("<meta http-equiv='refresh' content='0'>");
                                            echo "<script>alert('Data : " . $id . " dengan nama " . $nama . " dihapus')</script>";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="assets/script/js/datatables-simple-demo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" crossorigin="anonymous"></script>
</body>

</html>