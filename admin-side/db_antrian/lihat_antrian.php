<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
$res_antri = mysqli_query($koneksi, "SELECT * from antrian") or die(mysqli_error($koneksi));
$tanggal_antrian = "";
$jumlah_antrian = "";

$limit = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT IF(COUNT(DISTINCT tgl_masuk) <= 7, 7, COUNT(DISTINCT tgl_masuk)) as maksimal FROM antrian;"));
$limit_num = $limit['maksimal'] - 7;
$chart_antrian = mysqli_query($koneksi, "SELECT tgl_masuk AS Tanggal, COUNT('nomor antrian') AS Jumlah FROM antrian GROUP BY tgl_masuk limit " . $limit_num . ", 7") or die(mysqli_error($koneksi));
while ($row = mysqli_fetch_array($chart_antrian)) {
    $tanggal_antrian = $tanggal_antrian . '"' . $row['Tanggal'] . '",';
    $jumlah_antrian = $jumlah_antrian . '"' . $row['Jumlah'] . '",';
}
$tanggal_antrian = trim($tanggal_antrian, ",");
$jumlah_antrian = trim($jumlah_antrian, ",");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid px-4">
        <!-- title page -->
        <p>
        <h1 style="text-align: center;">Daftar Data Antrian</h1>
        <hr>
        </p>

        <!-- konten website -->

        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-chart-area me-1"></i>
                Jumlah Kunjungan
            </div>
            <div class="card-body"><canvas id="chart-antrian" style="width: 100%; height: auto;"></canvas></div>
        </div>


        <!-- daftar antrian -->
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Tabel Data Antrian
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nomor</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Gol. Darah</th>
                            <th>Keluhan</th>
                            <th>Nomor Telepon</th>
                            <th>Status</th>
                            <th>Tanggal Masuk</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Nomor</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Gol. Darah</th>
                            <th>Keluhan</th>
                            <th>Nomor Telepon</th>
                            <th>Status</th>
                            <th>Tanggal Masuk</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php
                        if (mysqli_num_rows($res_antri) > 0) {
                            while ($data = mysqli_fetch_assoc($res_antri)) {
                                $status_antri = '';
                                if ($data['status_antrian'] == 1) {
                                    $status_antri = "Selesai";
                                } else {
                                    $status_antri = "Belum Selesai";
                                }
                                echo "
                                <tr>                                                                    
                                    <td>" . $data['id_antrian'] . "</td>    
                                    <td>" . $data['no_antrian'] . "</td>
                                    <td>" . $data['nama_pengunjung'] . "</td>
                                    <td>" . $data['jenis_kelamin'] . "</td>
                                    <td>" . $data['usia'] . "</td>
                                    <td>" . $data['goldar'] . "</td>                                
                                    <td>" . $data['keluhan'] . "</td>
                                    <td>" . $data['no_telp'] . "</td>
                                    <td>" . $status_antri . "</td>
                                    <td>" . $data['tgl_masuk'] . "</td>
                                    <td> 
                                        <form method='post'>
                                            <a class='btn btn-primary' style='width: 80px;' name='edit' href='edit_antrian.php?id_antrian=" . $data['id_antrian'] . "' >Edit</a>
                                            <button class='btn btn-danger' style='width: 80px;' method='post' type='submit' name='delete' value='" . $data['id_antrian'] . "'>Hapus</button>                                        
                                        </form>  
                                    </td>
                                    
                                </tr>
                                ";
                            }
                        } else {
                            echo "0 results";
                        } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php

    if (isset($_POST['delete'])) {
        $id = $_POST['delete'];
        mysqli_query($koneksi, "DELETE FROM antrian WHERE id_antrian =  " . $id . ";");
        echo ("<meta http-equiv='refresh' content='0'>");
        echo "<script>alert('Data : " . $id . " dihapus')</script>";
    }
    ?>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>
    <script>
        // draw database ke chart antrian
        var ctx = document.getElementById("chart-antrian").getContext('2d');
        var ChartAntrian = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [<?php echo $tanggal_antrian ?>],
                datasets: [{
                    label: 'Jumlah Antrian Masuk',
                    lineTension: 0.3,
                    backgroundColor: "rgba(2,117,216,0.2)",
                    borderColor: "rgba(2,117,216,1)",
                    pointRadius: 5,
                    pointBackgroundColor: "rgba(2,117,216,1)",
                    pointBorderColor: "rgba(255,255,255,0.8)",
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(2,117,216,1)",
                    pointHitRadius: 50,
                    pointBorderWidth: 2,
                    data: [<?php echo $jumlah_antrian ?>],
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'date'
                        },
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            maxTicksLimit: 7
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 20,
                            maxTicksLimit: 5
                        },
                        gridLines: {
                            color: "rgba(0, 0, 0, .125)",
                        }
                    }],
                },
                legend: {
                    display: false
                }
            }
        });
    </script>
</body>

</html>