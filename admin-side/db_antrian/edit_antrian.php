<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
$id = $_GET['id_antrian'];
if ($id != NULL) {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM antrian WHERE id_antrian = " . $id . " ;"));
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-8 col-sm-10">
                <h1 class="mt-4">Edit data antrian</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="lihat_antrian.php">Daftar Antrian</a></li>
                    <li class="breadcrumb-item active">Antrian ID : <?php echo $result['id_antrian'] ?></li>
                </ol>
            </div>
            <!-- style tabel -->
            <style>
                input {
                    width: 80%;
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }

                td {
                    padding: 0px 10px;
                }
            </style>

            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Antrian
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form id="form-pendaftaran" method="post">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 20%;">
                                        <p> <label for="nama">Nama</label></p>
                                    </td>
                                    <td style="width: 80%;">
                                        <p> <input name="nama" id="nama" type="text" value="<?php echo $result['nama_pengunjung'] ?>" required /> </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="jenisKelamin">Jenis Kelamin</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="jenisKelamin" id="jenisKelamin" required>
                                                <option value="Laki - Laki">Laki - Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="goldar">Golongan Darah </label> </p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="goldar" id="goldar" required>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="AB">AB</option>
                                                <option value="O">O</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p> <label for="usia">Usia</label> </p>
                                    </td>
                                    <td>
                                        <p><input name="usia" id="usia" type="number" value="<?php echo $result['usia'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="keluhan">Keluhan</label></p>
                                    </td>
                                    <td>
                                        <p><input name="keluhan" id="keluhan" type="text" value="<?php echo $result['keluhan'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="telepon">No. Telp</label></p>
                                    </td>
                                    <td>
                                        <p><input name="telepon" id="telepon" type="text" value="<?php echo $result['no_telp'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="tgl_masuk">Tanggal Masuk</label></p>
                                    </td>
                                    <td>
                                        <p><input name="tgl_masuk" id="tgl_masuk" type="date" style="width: auto;" value="<?php echo $result['tgl_masuk'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="status">Status</label></p>
                                    </td>
                                    <td>
                                        <?php
                                        if ($result['status_antrian'] == 1) {
                                            $status = 'Selesai';
                                        } else {
                                            $status = 'Belum selesai';
                                        } ?>
                                        <p>
                                            <input name="status" id="status" type="text" value="<?php echo $status ?>" required />
                                        </p>
                                    </td>
                                </tr>
                            </table>

                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="submit" type="submit" name="edit_antrian">
                                    Edit Antrian
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_antrian.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['edit_antrian'])) {
                        $name = $_POST['nama'];
                        $jk = $_POST['jenisKelamin'];
                        $goldar = $_POST['goldar'];
                        $usia = $_POST['usia'];
                        $keluhan = $_POST['keluhan'];
                        $no_telp = $_POST['telepon'];
                        $tgl_msk = $_POST['tgl_masuk'];
                        $status = $_POST['status'];
                        if ($status == 'Selesai') {
                            $stat = 1;
                        } else {
                            $stat = 0;
                        }

                        mysqli_query($koneksi, "UPDATE antrian SET nama_pengunjung = '" . $name . "' WHERE id_antrian = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE antrian SET jenis_kelamin = '" . $jk . "' WHERE id_antrian = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE antrian SET usia = '" . $usia . "' WHERE id_antrian = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE antrian SET goldar = '" . $goldar . "' WHERE id_antrian = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE antrian SET keluhan = '" . $keluhan . "' WHERE id_antrian = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE antrian SET no_telp = '" . $no_telp . "' WHERE id_antrian = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE antrian SET tgl_masuk = '" . $tgl_msk . "' WHERE id_antrian = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE antrian SET status_antrian = '" . $stat . "' WHERE id_antrian = " . $id . ";");
                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Data Antrian " . $name . " Diupdate')</script>");
                    }

                    ?>
                </div>
            </div>
        </div>

    </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>

    <script type="text/javascript">
        function selectElement(id, valueToSelect) {
            let element = document.getElementById(id);
            element.value = valueToSelect;
        }

        selectElement('jenisKelamin', '<?php echo $result['jenis_kelamin'] ?>');
        selectElement('goldar', '<?php echo $result['goldar'] ?>');
    </script>

</body>

</html>