<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid px-4">
        <!-- title page -->
        <p>
        <h1 style="text-align: center;">Daftar Data Berita</h1>
        <hr>
        </p>

        <!-- konten website -->

        <!-- daftar petugas -->
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Tabel Data Berita
            </div>

            <style>
                .wrapText {
                    overflow: hidden;
                    text-overflow: ellipsis;
                    display: -webkit-box;
                    -webkit-line-clamp: 5;
                    -webkit-box-orient: vertical;
                }

                img {
                    height: auto;
                    width: 300px;
                }
            </style>

            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Isi Berita</th>
                            <th>Tanggal Upload</th>
                            <th>Lampiran</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Isi Berita</th>
                            <th>Tanggal Upload</th>
                            <th>Lampiran</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php
                        $res_berita = mysqli_query($koneksi, "SELECT * FROM berita;");
                        if (mysqli_num_rows($res_berita) > 0) {
                            while ($row = mysqli_fetch_assoc($res_berita)) {
                        ?>
                                <tr>
                                    <td style="width: 5%;"><?php echo $row['nomor_berita'] ?></td>
                                    <td style="width: 15%;"><?php echo $row['judul_berita'] ?></td>
                                    <td style="width: 30%;">
                                        <span class="wrapText">
                                            <?php echo $row['isi_berita'] ?>
                                        </span>
                                    </td>
                                    <td style="width: 10%;"><?php echo $row['tgl_berita'] ?></td>
                                    <td class="text-center" style="width: 25%;">
                                        <?php echo '<img src="data:image/jpeg;base64,' . base64_encode($row['img_data']) . '"/>'; ?>
                                    </td>
                                    <td style="width: 20%;">
                                        <form method='post'>
                                            <a class='btn btn-primary' style='width: 80px;' name='edit' href="edit_berita.php?id_berita='<?php echo $row['nomor_berita'] ?>'">Edit</a>
                                            <button class='btn btn-danger' style='width: 80px;' method='post' type='submit' name='delete' value="<?php echo $row['nomor_berita'] ?>">Hapus</button>
                                            <input type='hidden' name='judul' value=" <?php echo $row['judul_berita'] ?>">
                                        </form>
                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <?php

    if (isset($_POST['delete'])) {
        $id = $_POST['delete'];
        $nama = $_POST['judul'];
        mysqli_query($koneksi, "DELETE FROM berita WHERE nomor_berita =  " . $id . ";");
        echo ("<meta http-equiv='refresh' content='0'>");
        echo "<script>alert('Data : " . $id . " dengan nama " . $nama . " dihapus')</script>";
    }

    ?>

    <script src=" ../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>
</body>

</html>