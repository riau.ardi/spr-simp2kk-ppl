<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
// nilai maksimal Id pasien
$result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(nomor_berita) FROM berita;"));
$maks_id = $result['MAX(nomor_berita)'] + 1;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-2 col-sm-10"></div>
            <div class="col-xl-10 col-sm-10">
                <h1 class="mt-4">Data Dokumen Berita</h1>
                <ol class="breadcrumb mb-4 ">
                    <li class="breadcrumb-item"><a href="lihat_berita.php">Daftar Dokumen Berita</a></li>
                    <li class="breadcrumb-item active">Berita ID : <?php echo $maks_id ?></li>
                </ol>
            </div>

            <style>
                input {
                    width: 100%;
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }
            </style>
            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Berita baru
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form id="form-pendaftaran" method="post" enctype="multipart/form-data">
                            <h3>Judul Berita</h3>
                            <p><input name="judul" id="judul" style="margin-bottom : 10px;" type="text" required /></p>
                            <div class="input-group mb-3">
                                <label class="input-group-text" for="lampiran">Foto Berita</label>
                                <input type="file" class="form-control" id="lampiran" name="lampiran">
                            </div>
                            <p> <textarea name="isi" id="isi"></textarea></p>

                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="submit" type="submit" name="tambah_berita">
                                    Upload Berita
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_berita.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['tambah_berita'])) {
                        $name = $_POST['judul'];
                        $isi = $_POST['isi'];
                        $tgl =  date('Y-m-d H:i:s', time() + 7 * 60 * 60);
                        $name_f = $_FILES['lampiran']['name'];
                        $type_f = $_FILES['lampiran']['type'];
                        $file_f = file_get_contents($_FILES['lampiran']['tmp_name']);

                        $quer = $pdo_koneksi->prepare(
                            "INSERT INTO berita(judul_berita, isi_berita, tgl_berita, img_nama, img_tipe, img_data) 
                                    VALUES('" . $name . "','" . $isi . "','" . $tgl . "',?,?,?);"
                        );
                        $quer->bindParam(1, $name_f);
                        $quer->bindParam(2, $type_f);
                        $quer->bindParam(3, $file_f);
                        $quer->execute();

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Berita " . $name . " diupload')</script>");
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>

    <script src="../assets/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('isi');
    </script>

</body>

</html>