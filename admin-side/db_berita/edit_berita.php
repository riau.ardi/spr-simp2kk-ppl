<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');

$id = $_GET['id_berita'];
if ($id != 0) {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM berita WHERE nomor_berita = " . $id . " ;"));
} else {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(nomor_berita) FROM berita;"));
    $maks_id = $result['MAX(nomor_berita)'];
    $result['nomor_berita'] = $maks_id + 1;
    $result['judul_berita'] = '';
    $result['isi_berita'] = '';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- style tabel -->
            <style>
                input {
                    width: 100%;
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }
            </style>

            <!-- title page -->
            <div class="col-xl-8 col-sm-10">
                <h1 class="mt-4">Data Berita</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="lihat_berita.php">Daftar Berita</a></li>
                    <li class="breadcrumb-item active">ID Berita: <?php echo $result['nomor_berita'] ?></li>
                </ol>
            </div>

            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Petugas
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <?php echo "<img class='img-fluid rounded' style='width:100%; height:300px; object-fit: cover; margin-bottom:20px;' src='data:image/jpeg;base64," . base64_encode($result['img_data']) . "'/>" ?>
                        <form id="form-pendaftaran" method="post" enctype="multipart/form-data">

                            <div class="d-flex justify-content-center">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#ganti_foto">
                                    Ganti Foto
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="ganti_foto" tabindex="-1" aria-labelledby="ganti_fotoLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Upload foto baru</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="input-group mb-3">
                                                    <label class="input-group-text" for="lampiran">Foto Berita</label>
                                                    <input type="file" class="form-control" id="lampiran" name="lampiran">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" id="edit_foto" name="edit_foto">Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3>Judul Berita</h3>
                            <p><input name="judul" id="judul" style="margin-bottom: 10px;" type="text" value="<?php echo $result['judul_berita'] ?>" required /> </p>
                            <p><textarea name="isi" id="isi"><?php echo $result['isi_berita'] ?></textarea></p>

                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="edit_berita" type="submit" name="edit_berita">
                                    Update Berita
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_berita.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['edit_foto'])) {
                        $name_f = $_FILES['lampiran']['name'];
                        $type_f = $_FILES['lampiran']['type'];
                        $file_f = file_get_contents($_FILES['lampiran']['tmp_name']);

                        $quer = $pdo_koneksi->prepare(
                            "UPDATE berita SET 
                                img_nama = :nama_f,
                                img_tipe = :tipe_f,
                                img_data = :data_f
                                WHERE nomor_berita = " . $id . ";"
                        );
                        $quer->bindParam('nama_f', $name_f);
                        $quer->bindParam('tipe_f', $name_f);
                        $quer->bindParam('data_f', $file_f, PDO::PARAM_LOB);
                        $quer->execute();

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Foto " . $name . " diupdate')</script>");
                    }

                    if (isset($_POST['edit_berita'])) {
                        $judul = $_POST['judul'];
                        $isi = $_POST['isi'];
                        $tgl =  date('Y-m-d H:i:s');

                        mysqli_query($koneksi, "UPDATE berita SET 
                        judul_berita = '" . $judul . "', 
                        isi_berita = '" . $isi . "', 
                        tgl_berita = '" . $tgl . "' 
                        WHERE nomor_berita = " . $id . " ;") or die(mysqli_error($koneksi));;
                    } ?>
                </div>
            </div>
        </div>

    </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>

    <script type="text/javascript">
        function selectElement(id, valueToSelect) {
            let element = document.getElementById(id);
            element.value = valueToSelect;
        }

        selectElement('nomor', '<?php echo $result['nomor_rekap'] ?>');
        selectElement('nama', '<?php echo $result['judul_rekap'] ?>');
        selectElement('deskripsi', '<?php echo $result['deskripsi_rekap'] ?>');
    </script>
    <script src="../assets/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('isi');
    </script>
</body>

</html>