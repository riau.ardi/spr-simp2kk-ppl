<?php
include('../config.php');

$id = isset($_GET['id_dokumen']) ? $_GET['id_dokumen'] : "";
$res = $pdo_koneksi->prepare("SELECT * FROM `rekap` WHERE id_rekap =? ");
$res->bindParam(1, $id);
$res->execute();
$row = $res->fetch();
header('Content-Type:' . $row['tipe_file']);
echo $row['lampiran_file'];
