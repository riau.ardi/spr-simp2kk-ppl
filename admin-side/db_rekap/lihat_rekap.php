<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid px-4">
        <!-- title page -->
        <p>
        <h1 style="text-align: center;">Daftar Data Rekap</h1>
        <hr>
        </p>

        <!-- konten website -->

        <!-- daftar petugas -->
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Tabel Data Rekap
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nomor</th>
                            <th> Judul</th>
                            <th>Deskripsi</th>
                            <th>Tipe Dokumen</th>
                            <th>Lampiran</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Nomor</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Tipe Dokumen</th>
                            <th>Lampiran</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php
                        $res_rekap = mysqli_query($koneksi, "SELECT * FROM rekap;");
                        if (mysqli_num_rows($res_rekap) > 0) {
                            while ($row = mysqli_fetch_assoc($res_rekap)) {
                        ?>
                                <tr>
                                    <td style="width: 5%;"><?php echo $row['id_rekap'] ?></td>
                                    <td style="width: 10%;"><?php echo $row['nomor_rekap'] ?></td>
                                    <td style="width: 20%;"><?php echo $row['judul_rekap'] ?></td>
                                    <td style="width: 30%;"><?php echo $row['deskripsi_rekap'] ?></td>
                                    <td style="width: 10%;"><?php echo $row['tipe_file'] ?></td>
                                    <td class="text-center">
                                        <a href="show.php?id_dokumen=<?php echo $row['id_rekap'] ?>" class="btn btn-outline-secondary" style="width: 100%;" target="_blank"><?php echo $row['nama_file'] ?></a>
                                    </td>
                                    <td style="width: 20%;">
                                        <form method='post'>
                                            <a class='btn btn-primary' style='width: 80px;' name='edit' href="edit_rekap.php?id_rekap='<?php echo $row['id_rekap'] ?>'">Edit</a>
                                            <button class='btn btn-danger' style='width: 80px;' method='post' type='submit' name='delete' value="<?php echo $row['id_rekap'] ?>">Hapus</button>
                                            <input type='hidden' name='judul' value=" <?php echo $row['judul_rekap'] ?>">
                                        </form>
                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <?php

    if (isset($_POST['delete'])) {
        $id = $_POST['delete'];
        $nama = $_POST['judul'];
        mysqli_query($koneksi, "DELETE FROM rekap WHERE id_rekap =  " . $id . ";");
        echo ("<meta http-equiv='refresh' content='0'>");
        echo "<script>alert('Data : " . $id . " dengan nama " . $nama . " dihapus')</script>";
    }

    ?>

    <script src=" ../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>
</body>

</html>