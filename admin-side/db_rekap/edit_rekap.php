<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');

$id = $_GET['id_rekap'];
if ($id != 0) {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM rekap WHERE id_rekap = " . $id . " ;"));
} else {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(id_rekap) FROM rekap;"));
    $maks_id = $result['MAX(id_rekap)'];
    $result['id_rekap'] = $maks_id + 1;
    $result['nomor_rekap'] = '';
    $result['judul_rekap'] = '';
    $result['deskripsi_rekap'] = '';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-8 col-sm-10">
                <h1 class="mt-4">Data Rekap</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="lihat_rekap.php">Daftar Rekap</a></li>
                    <li class="breadcrumb-item active">Rekap ID : <?php echo $result['id_rekap'] ?></li>
                </ol>
            </div>
            <!-- style tabel -->
            <style>
                input {
                    width: 80%;
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }

                textarea {
                    border-top-style: groove;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                    border-bottom-width: 2px;
                    border-top-width: 2px;
                }

                .new-btn {
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: hidden;
                    background-color: rgba(0, 0, 0, 0.0);
                    color: white;
                    height: 100%;
                }

                .input-group-text {
                    background-color: #0d6efd;
                }

                .input-group-text:hover {
                    background-color: #0d6efd;
                }
            </style>

            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Petugas
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form id="form-pendaftaran" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-xl-6 col-sm-10">
                                    <p>
                                        <b> <label for="nomor">Nomor Dokumen</label> </b> <br>
                                        <input name="nomor" id="nomor" type="text" value="<?php echo $result['nomor_rekap'] ?>" required />
                                    </p>
                                </div>
                                <div class="col-xl-6 col-sm-10">
                                    <p>
                                        <b><label for="nama">Nama Dokumen</label></b> <br>
                                        <input name="nama" id="nama" type="text" value="<?php echo $result['judul_rekap'] ?>" required />
                                    </p>
                                </div>
                            </div>

                            <p><b>Dokumen saat ini : <?php echo $result['nama_file'] ?></b></p>
                            <div class="input-group mb-3">
                                <input type="file" class="form-control" id="lampiran" name="lampiran">
                                <label class="input-group-text" for="lampiran">
                                    <button class="new-btn" id="edit_doku" type="submit" name="edit_doku"> Update Dokumen</button>
                                </label>
                            </div>

                            <p> <textarea name="deskripsi" id="isi"> <?php echo $result['deskripsi_rekap'] ?> </textarea></p>

                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="edit_rekap" type="submit" name="edit_rekap">
                                    Upload Dokumen
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_rekap.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['edit_doku'])) {
                        $name_f = $_FILES['lampiran']['name'];
                        $type_f = $_FILES['lampiran']['type'];
                        $file_f = file_get_contents($_FILES['lampiran']['tmp_name']);

                        $quer = $pdo_koneksi->prepare(
                            "UPDATE rekap SET 
                                tipe_file= :tipe_f,
                                lampiran_file = :lampiran_f,
                                nama_file = :nama_f
                                WHERE id_rekap = " . $id . ";"
                        );
                        $quer->bindParam('tipe_f', $type_f);
                        $quer->bindParam('lampiran_f', $file_f, PDO::PARAM_LOB);
                        $quer->bindParam('nama_f', $name_f);
                        $quer->execute();

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Dokumen " . $name . " diupdate')</script>");
                    }

                    if (isset($_POST['edit_rekap'])) {
                        $nomor = $_POST['nomor'];
                        $name = $_POST['nama'];
                        $desc = $_POST['deskripsi'];


                        mysqli_query($koneksi, "UPDATE rekap SET 
                        nomor_rekap = '" . $nomor . "', 
                        judul_rekap = '" . $name . "', 
                        deskripsi_rekap = '" . $desc . "' 
                        WHERE id_rekap = " . $id . " ;") or die(mysqli_error($koneksi));;

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Dokumen " . $name . " diupdate')</script>");
                    } ?>
                </div>
            </div>
        </div>

    </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>
    <script src="../assets/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('isi');
    </script>

</body>

</html>