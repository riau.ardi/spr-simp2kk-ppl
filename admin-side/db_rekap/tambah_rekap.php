<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
// nilai maksimal Id pasien
$result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(id_rekap) FROM rekap;"));
$maks_id = $result['MAX(id_rekap)'] + 1;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-2 col-sm-10"></div>
            <div class="col-xl-10 col-sm-10">
                <h1 class="mt-4">Data Dokumen</h1>
                <ol class="breadcrumb mb-4 ">
                    <li class="breadcrumb-item"><a href="lihat_rekap.php">Daftar Dokumen</a></li>
                    <li class="breadcrumb-item active">Dokumen ID : <?php echo $maks_id ?></li>
                </ol>
            </div>

            <style>
                input {
                    width: 100%;
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }

                textarea {
                    border-top-style: groove;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                    border-bottom-width: 2px;
                    border-top-width: 2px;
                }

                td {
                    padding: 0px 10px;
                }
            </style>
            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Dokumen baru
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form id="form-pendaftaran" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-xl-6 col-sm-10">
                                    <p>
                                        <b> <label for="nomor">Nomor Dokumen</label> </b> <br>
                                        <input name="nomor" id="nomor" type="text" value="" required />
                                    </p>
                                </div>
                                <div class="col-xl-6 col-sm-10">
                                    <p>
                                        <b><label for="nama">Nama Dokumen</label></b> <br>
                                        <input name="nama" id="nama" type="text" value="" required />
                                    </p>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <label class="input-group-text" for="lampiran">Upload Dokumen</label>
                                <input type="file" class="form-control" id="lampiran" name="lampiran">
                            </div>

                            <p> <textarea name="deskripsi" id="isi">  </textarea></p>

                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="submit" type="submit" name="tambah_dokunen">
                                    Upload Dokumen
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_rekap.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['tambah_dokunen'])) {
                        $nomor = $_POST['nomor'];
                        $name = $_POST['nama'];
                        $desc = $_POST['deskripsi'];
                        $name_f = $_FILES['lampiran']['name'];
                        $type_f = $_FILES['lampiran']['type'];
                        $file_f = file_get_contents($_FILES['lampiran']['tmp_name']);

                        $quer = $pdo_koneksi->prepare(
                            "INSERT INTO rekap(nomor_rekap, judul_rekap, deskripsi_rekap, tipe_file, lampiran_file, nama_file) 
                            VALUES('" . $nomor . "','" . $name . "','" . $desc . "',?,?,?);"
                        );
                        $quer->bindParam(1, $type_f);
                        $quer->bindParam(2, $file_f);
                        $quer->bindParam(3, $name_f);
                        $quer->execute();

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Dokumen " . $name . " diupload')</script>");
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>


    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>
    <script src="../assets/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('isi');
    </script>

</body>

</html>