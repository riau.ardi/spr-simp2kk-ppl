<?php
session_start();
include('config.php');

// cek cookie
if (isset($_COOKIE['id']) && isset($_COOKIE['key'])) {
    $id = $_COOKIE['id'];
    $key = $_COOKIE['key'];

    //cek username cookie
    $result = mysqli_query($koneksi, "SELECT username FROM akun WHERE id =" . $id . " ");
    $row = mysqli_fetch_assoc($result);

    // cek kesamaan
    if ($key === hash('sha256', $row['username'])) {
        $_SESSION['login'] = true;
        $_SESSION['userlogin'] = $row['username'];
    }
}

if (isset($_SESSION['login'])) {
    // $_SESSION['userlogin'] = $_COOKIE['userlogin'];
    header("Location: index.php");
    exit;
}

if (isset($_POST['login'])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    // cek username
    $result =  mysqli_query($koneksi, "SELECT * FROM akun WHERE username = '" . $username . "'; ");

    // cek password
    if (mysqli_num_rows($result) === 1) {
        $row =  mysqli_fetch_assoc($result);
        if (password_verify($password, $row['password'])) {

            // set session
            $_SESSION['login'] = true;
            $_SESSION['userlogin'] = $username;

            // simpan login
            if (isset($_POST['remember'])) {
                setcookie('id', $row['id'], time() + 3600);
                setcookie('key', hash('sha256', $row['username']), time() + 60);
            }

            header("Location: index.php");
            exit;
        }
    }
    $error = true;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/logo-small.png" type="image/x-icon">
    <title>Login SIMP2KK</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
</head>

<body style="background-image: url(assets/images/hospi.jpg); background-size: 100%;">
    <div class="container" style="margin-top: 70px;">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-sm-10">
                <div class="card">
                    <div class="text-center mt-5 mb-5">
                        <img src="assets/images/logo-blue.png" alt="logo login" width="60%">
                    </div>

                    <style>
                        td {
                            padding: 10px;
                        }

                        label {
                            font-family: serif;
                            font-size: 18px;
                        }
                    </style>

                    <div class="text-center">
                        <div class="mb-3">
                            <h2>Login SIMP2KK</h2>
                        </div>
                        <div style="text-align: left; padding:0px 30px; margin-bottom: 80px;">

                            <?php if (isset($error)) : ?>
                                <div class="alert alert-danger">Username atau Password salah</div>
                            <?php endif ?>

                            <form action="" method="post">
                                <table class="" style="width: 100%;">
                                    <tr>
                                        <td><label for="username">Username </label></td>
                                        <td><input type="text" name="username" id="username"></td>
                                    </tr>
                                    <tr>
                                        <td><label for="password">Password </label></td>
                                        <td><input type="password" name="password" id="password"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="remember" name="remember" value="">
                                                <label class="form-check-label" for="remember">
                                                    Simpan Login
                                                </label>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                                <div class="d-flex justify-content-end" style="padding-right: 12px;">
                                    <button type="submit" class="btn btn-primary" name="login">login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>