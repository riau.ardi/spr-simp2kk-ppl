<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');

$id = $_GET['id_petugas'];
if ($id != 0) {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM petugas WHERE nomor_petugas = " . $id . " ;"));
} else {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(nomor) FROM pasien;"));
    $maks_id = $result['MAX(nomor)'];
    $result['nomor_petugas'] = $maks_id + 1;
    $result['nama_petugas'] = '';
    $result['jk_petugas'] = 'Laki - Laki';
    $result['bidang_petugas'] = '';
    $result['jadwal_petugas'] = '';
    $result['jam_kerja'] = '';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-8 col-sm-10">
                <h1 class="mt-4">Data Petugas</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="lihat_petugas.php">Daftar Petugas</a></li>
                    <li class="breadcrumb-item active">Petugas ID : <?php echo $result['nomor_petugas'] ?></li>
                </ol>
            </div>
            <!-- style tabel -->
            <style>
                input {

                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }

                td {
                    padding: 0px 10px;
                }

                #main {
                    display: flex;
                    justify-content: left;
                }
            </style>

            <!-- konten website -->
            <div class="col-xl-8 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Petugas
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form id="form-pendaftaran" method="post">
                            <table class="" style="width: 100%;">
                                <tr>
                                    <td style="width: 20%;">
                                        <p> <label for="nama">Nama petugas</label></p>
                                    </td>
                                    <td colspan="2" style="width: 80%;">
                                        <p> <input name="nama" id="nama" style="width: 80%;" type="text" value="<?php echo $result['nama_petugas'] ?>" required /> </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p><label for="jenisKelamin">Jenis Kelamin</label></p>
                                    </td>
                                    <td colspan="2">
                                        <p>
                                            <select name="jenisKelamin" id="jenisKelamin" required>
                                                <option value="Laki - Laki">Laki - Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p><label for="bidang">Bidang Petugas</label></p>
                                    </td>
                                    <td colspan="2">
                                        <p>
                                            <select name="bidang" id="bidang" required>
                                                <option value="Spesialis THT">Spesialis THT</option>
                                                <option value="Spesialis Penyakit Dalam">Spesialis Penyakit Dalam</option>
                                                <option value="Spesialis Penyakit Luar">Spesialis Penyakit Luar</option>
                                                <option value="Spesialis Gizi">Spesialis Gizi</option>
                                                <option value="Spesialis Mata">Spesialis Mata</option>
                                                <option value="Spesialis Gigi">Spesialis Gigi</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p><label for="jadwal">Jadwal Kerja</label></p>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-4 d-flex flex-column justify-content-start">
                                                <label><input type="checkbox" name="jadwal[]" value="Senin" <?php echo (str_contains($result['jadwal_petugas'], "Senin")  ? 'checked' : ''); ?>> Senin</label>
                                                <label><input type="checkbox" name="jadwal[]" value="Selasa" <?php echo (str_contains($result['jadwal_petugas'], "Selasa")  ? 'checked' : ''); ?>> Selasa</label>
                                                <label><input type="checkbox" name="jadwal[]" value="Rabu" <?php echo (str_contains($result['jadwal_petugas'], "Rabu")  ? 'checked' : ''); ?>> Rabu</label>
                                            </div>
                                            <div class="col-4 d-flex flex-column justify-content-start">
                                                <label><input type="checkbox" name="jadwal[]" value="Kamis" <?php echo (str_contains($result['jadwal_petugas'], "Kamis")  ? 'checked' : ''); ?>> Kamis</label>
                                                <label><input type="checkbox" name="jadwal[]" value="Jumat" <?php echo (str_contains($result['jadwal_petugas'], "Jumat")  ? 'checked' : ''); ?>> Jumat</label>
                                                <label><input type="checkbox" name="jadwal[]" value="Sabtu" <?php echo (str_contains($result['jadwal_petugas'], "Sabtu")  ? 'checked' : ''); ?>> Sabtu</label>
                                            </div>
                                        </div>
                                        <br>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p><label for="jam">Jam Kerja</label></p>
                                    </td>
                                    <td colspan="3">
                                        <p>
                                            <select name="jam" id="jam" required>
                                                <option value="08.00 - 10.00">08.00 - 10.00</option>
                                                <option value="08.00 - 12.00">08.00 - 12.00</option>
                                                <option value="10.00 - 12.00">10.00 - 12.00</option>
                                                <option value="10.00 - 15.00">10.00 - 15.00</option>
                                                <option value="12.00 - 16.00">12.00 - 16.00</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                            </table>

                            <!-- form send -->
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="submit" type="submit" name="edit_petugas">
                                    Edit Antrian
                                </button>
                        </form>
                        <a class="btn btn-secondary" href="lihat_petugas.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['edit_petugas'])) {
                        $name = $_POST['nama'];
                        $jk = $_POST['jenisKelamin'];
                        $bidang = $_POST['bidang'];
                        $jam = $_POST['jam'];
                        $jadwalChk = $_POST['jadwal'];
                        $jadwal = "";
                        foreach ($jadwalChk as $jadwalChkValue) {
                            $jadwal .= $jadwalChkValue . ", ";
                        }

                        mysqli_query($koneksi, "UPDATE petugas SET nama_petugas = '" . $name . "' WHERE nomor_petugas = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE petugas SET jk_petugas = '" . $jk . "' WHERE nomor_petugas = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE petugas SET bidang_petugas = '" . $bidang . "' WHERE nomor_petugas = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE petugas SET jadwal_petugas = '" . $jadwal . "' WHERE nomor_petugas = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE petugas SET jam_petugas = '" . $jam . "' WHERE nomor_petugas = " . $id . ";");
                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Data Antrian " . $name . " Diupdate')</script>");
                    }

                    ?>
                </div>
            </div>
        </div>

    </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>

    <script type="text/javascript">
        function selectElement(id, valueToSelect) {
            let element = document.getElementById(id);
            element.value = valueToSelect;
        }

        selectElement('jenisKelamin', '<?php echo $result['jk_petugas'] ?>');
        selectElement('bidang', '<?php echo $result['bidang_petugas'] ?>');
        selectElement('jam', '<?php echo $result['jam_kerja'] ?>');
    </script>

</body>

</html>