<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
$res_petugas = mysqli_query($koneksi, "SELECT * from petugas") or die(mysqli_error($koneksi));
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid px-4">
        <!-- title page -->
        <p>
        <h1 style="text-align: center;">Daftar Data Petugas</h1>
        <hr>
        </p>

        <!-- konten website -->

        <!-- daftar petugas -->
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Tabel Data Petugas
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Nama Petugas</th>
                            <th>Jenis Kelamin</th>
                            <th>Bidang Petugas</th>
                            <th>Jadwal Petugas</th>
                            <th>Jam Kerja</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nomor</th>
                            <th>Nama Petugas</th>
                            <th>Jenis Kelamin</th>
                            <th>Bidang Petugas</th>
                            <th>Jadwal Petugas</th>
                            <th>Jam Kerja</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php
                        if (mysqli_num_rows($res_petugas) > 0) {
                            while ($data = mysqli_fetch_assoc($res_petugas)) {
                                echo "
                                <tr>                                                                                                         
                                    <td>" . $data['nomor_petugas'] . "</td>
                                    <td>" . $data['nama_petugas'] . "</td>
                                    <td>" . $data['jk_petugas'] . "</td>
                                    <td>" . $data['bidang_petugas'] . "</td>
                                    <td>" . $data['jadwal_petugas'] . "</td>                                
                                    <td>" . $data['jam_kerja'] . "</td>                                                                        
                                    <td> 
                                        <form method='post'>
                                            <a class='btn btn-primary' style='width: 80px;' name='edit' href='edit_petugas.php?id_petugas=" . $data['nomor_petugas'] . "' >Edit</a>
                                            <input type='hidden' name='nama' value='" . $data['nama_petugas'] . "'>
                                            <button class='btn btn-danger' style='width: 80px;' method='post' type='submit' name='delete' value='" . $data['nomor_petugas'] . "'>Hapus</button>                                        
                                        </form>  
                                    </td>                                    
                                </tr>
                                ";
                            }
                        } else {
                            echo "0 results";
                        } ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <?php

    if (isset($_POST['delete'])) {
        $id = $_POST['delete'];
        $nama = $_POST['nama'];
        mysqli_query($koneksi, "DELETE FROM petugas WHERE nomor_petugas =  " . $id . ";");
        mysqli_query($koneksi, "UPDATE pasien SET dokter_pasien = '' WHERE dokter_pasien LIKE '" . $nama . "';");
        echo ("<meta http-equiv='refresh' content='0'>");
        echo "<script>alert('Data : " . $id . " dengan nama " . $nama . " dihapus')</script>";
    }
    ?>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>
</body>

</html>