<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}
include('config.php')
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <link href="assets/script/css/styles.css" rel="stylesheet" />
    <link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <div class="card-header text-center">
                        <h2>
                            Daftar Masukan
                        </h2>
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Nomor</th>
                                    <th style="width: 60%;">Pesan</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Nomor</th>
                                    <th style="width: 60%;">Pesan</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                $res_masukan = mysqli_query($koneksi, "SELECT * FROM masukan ORDER BY id_masukan DESC;") or die(mysqli_error($koneksi));;

                                if (mysqli_num_rows($res_masukan) > 0) {
                                    while ($data = mysqli_fetch_assoc($res_masukan)) {
                                        echo "
                                        <tr>                                                                    
                                            <td>" . $data['id_masukan'] . "</td>
                                            <td>" . $data['nama_masukan'] . "</td>
                                            <td>" . $data['email_masukan'] . "</td>
                                            <td>" . $data['telp_masukan'] . "</td>
                                            <td>" . $data['pesan_masukan'] . "</td>                                                                                                                       
                                        </tr>
                                        ";
                                    }
                                } else {
                                    echo "0 results";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="assets/script/js/datatables-simple-demo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" crossorigin="anonymous"></script>
</body>

</html>