<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');

$id = $_GET['id_ruang'];
if ($id != 0) {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM ruangan WHERE id_ruang = " . $id . " ;"));
} else {
    $result = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(nomor) FROM ruangan;"));
    $maks_id = $result['MAX(nomor)'];
    $result['id_ruang'] = $maks_id + 1;
    $result['nama_pasien'] = '';
    $result['level_ruang'] = '1';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <title></title>
</head>

<body>
    <div class="container-fluid px-4">
        <div class="row justify-content-center">
            <!-- title page -->
            <div class="col-xl-7 col-sm-10">
                <h1 class="mt-4">Edit Data Ruangan</h1>
                <ol class="breadcrumb mb-4 ">
                    <li class="breadcrumb-item"><a href="lihat_ruang.php">Daftar Ruangan</a></li>
                    <li class="breadcrumb-item active">Ruangan ID : <?php echo $id ?></li>
                </ol>
            </div>

            <style>
                input {
                    border-top-style: hidden;
                    border-right-style: hidden;
                    border-left-style: hidden;
                    border-bottom-style: groove;
                }

                td {
                    padding: 0px 10px;
                }
            </style>
            <!-- konten website -->
            <div class="col-xl-7 col-sm-10">
                <div class="card mb-4">
                    <!-- form header -->
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Data Ruangan baru
                    </div>
                    <!-- form data -->
                    <div class="card-body">
                        <form action="" method="POST">
                            <table>
                                <tr>
                                    <td style="width: 40%;">
                                        <p><label for="nama">Nama Ruangan</label></p>
                                    </td>
                                    <td>
                                        <p><input name="nama" id="nama" type="text" value="<?php echo $result['nama_ruang'] ?>" required /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><label for="level">Level Ruangan</label></p>
                                    </td>
                                    <td>
                                        <p>
                                            <select name="level" id="level" required>
                                                <option value="1">BPJS Tingkat 1</option>
                                                <option value="2">BPJS Tingkat 2</option>
                                                <option value="3">BPJS Tingkat 3</option>
                                            </select>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            <div style="text-align: center; margin: 10px 0px;">
                                <button class="btn btn-primary" id="submit" type="submit" name="edit_ruang">
                                    Edit Data
                                </button>
                                <button class='btn btn-danger' style='width: 80px;' method='post' type='submit' name='delete' value='<?php echo $result['id_ruang'] ?>'>
                                    Hapus
                                </button>
                        </form>
                        <a class=" btn btn-secondary" href="lihat_ruang.php">Kembali</a>
                    </div>
                    <?php
                    if (isset($_POST['edit_ruang'])) {
                        $name = $_POST['nama'];
                        $level = $_POST['level'];

                        mysqli_query($koneksi, "UPDATE ruangan SET nama_ruang = '" . $name . "' WHERE id_ruang = " . $id . ";");
                        mysqli_query($koneksi, "UPDATE ruangan SET level_ruang = '" . $level . "' WHERE id_ruang = " . $id . ";");

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Data " . $name . " diupdate')</script>");
                    }
                    if (isset($_POST['delete'])) {
                        $id = $_POST['delete'];
                        $nama = $_POST['nama'];

                        mysqli_query($koneksi, "DELETE FROM ruangn WHERE id_ruang =  " . $id . ";");
                        mysqli_query($koneksi, "UPDATE pasien SET ruang_pasien = '' WHERE ruang_pasien LIKE '" . $nama . "';");

                        echo ("<meta http-equiv='refresh' content='0'>");
                        echo ("<script>alert('Data " . $nama . " dengan ID : " . $id . " dihapus')</script>");
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>

    <script type="text/javascript">
        function selectElement(id, valueToSelect) {
            let element = document.getElementById(id);
            element.value = valueToSelect;
        }

        selectElement('level', '<?php echo $result['level_ruang'] ?>');
    </script>
</body>

</html>