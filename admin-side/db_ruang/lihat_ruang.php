<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
}

include('../config.php');
$res_ruang = mysqli_query($koneksi, "SELECT * from ruangan ORDER BY level_ruang ASC;") or die(mysqli_error($koneksi));

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="../assets/script/css/styles.css" rel="stylesheet" />
    <link href="../node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>

<body>
    <style>
        input {
            border-top-style: hidden;
            border-right-style: hidden;
            border-left-style: hidden;
            border-bottom-style: groove;
        }

        a {
            color: black;
            text-decoration: none;
        }
    </style>
    <!-- content web -->
    <div class="container">
        <div class="card pt-3 p-3">
            <div class="row justify-content-center">
                <div class="text-center">
                    <h2 style="padding-bottom: 10px;">Daftar Ruangan</h2>
                    <hr>
                </div>

                <!-- tambah data -->
                <div class="col-xl-4 col-sm-10 mb-3">
                    <div class="card" id="tambah">
                        <div class="card-header">
                            <h5><b>Tambah Data Ruang</b></h5>
                        </div>
                        <div class="card-body">
                            <form action="" method="POST">
                                <table>
                                    <tr>
                                        <td style="width: 40%;">
                                            <p><label for="nama">Nama Ruangan</label></p>
                                        </td>
                                        <td>
                                            <p><input name="nama" id="nama" type="text" required /></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><label for="level">Level Ruangan</label></p>
                                        </td>
                                        <td>
                                            <p>
                                                <select name="level" id="level" required>
                                                    <option value="1">BPJS Tingkat 1</option>
                                                    <option value="2">BPJS Tingkat 2</option>
                                                    <option value="3">BPJS Tingkat 3</option>
                                                </select>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <div style="text-align: center; margin: 10px 0px;">
                                    <button class="btn btn-primary" id="submit" type="submit" name="tambah_ruang">
                                        Tambah Data
                                    </button>
                                </div>
                            </form>
                            <?php
                            if (isset($_POST['tambah_ruang'])) {
                                $name = $_POST['nama'];
                                $level = $_POST['level'];

                                mysqli_query($koneksi, "INSERT INTO ruangan (nama_ruang, level_ruang)
                                VALUES ('" . $name . "', '" . $level . "');") or die(mysqli_error($koneksi));

                                echo ("<meta http-equiv='refresh' content='0'>");
                                echo ("<script>alert('Data " . $name . " Ditambahkan')</script>");
                            }
                            ?>

                        </div>
                    </div>
                </div>

                <!-- Daftar data -->
                <div class="col-xl-8 col-sm-10">
                    <?php
                    if (mysqli_num_rows($res_ruang) > 0) {
                        while ($data_ruang = mysqli_fetch_assoc($res_ruang)) {
                            if ($data_ruang['level_ruang'] == 1) {
                                $level_ruang = 'BPJS Tingkat 1';
                            } elseif ($data_ruang['level_ruang'] == 2) {
                                $level_ruang = 'BPJS Tingkat 2';
                            } elseif ($data_ruang['level_ruang'] == 3) {
                                $level_ruang = 'BPJS Tingkat 3';
                            }

                            echo "
                        <div class='card'>
                            <div class='card-header'>
                                <a href='edit_ruang.php?id_ruang=" . $data_ruang['id_ruang'] . "'>                     
                                    <b>" . $level_ruang . "</b>
                                    <h5><b> " . $data_ruang['nama_ruang'] . "</b></h5>
                                </a>                                                       
                            </div>
                            <div class='card-body'>
                                <table class='table'>
                                    <tr>
                                        <th style='width: 60%;'>Nama Pasien</th>
                                        <th style='width: 40%;'>Dokter Pasien</th>
                                    </tr>
                                ";
                            $res_pasien_ruang = mysqli_query($koneksi, "SELECT * FROM `pasien` WHERE ruang_pasien LIKE '%" . $data_ruang['nama_ruang'] . "%';") or die(mysqli_error($koneksi));
                            if (mysqli_num_rows($res_pasien_ruang) > 0) {
                                while ($data_pasien_ruang = mysqli_fetch_assoc($res_pasien_ruang)) {
                                    echo "
                                    <tr>
                                        <td>" . $data_pasien_ruang['nama_pasien'] . "</td>
                                        <td style=''>" . $data_pasien_ruang['dokter_pasien'] . "</td>
                                    </tr>                                    
                                ";
                                }
                            }
                            echo "                                    
                                </table>
                            </div>
                        </div>
                        <br>
                        ";
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <script src=" ../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/script/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="../assets/script/js/datatables-simple-demo.js"></script>
</body>

</html>